import { HttpServer } from '../../data-structures/http-server'

const httpServerTable: { [httpServerId: string]: HttpServer } = {}

export {
  httpServerTable
}

