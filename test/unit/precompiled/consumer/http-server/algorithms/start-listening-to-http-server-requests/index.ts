

import { httpServerTable } from '../../variables/http-server'


function startListeningToHttpServerRequestsFunction (httpServerId: string, portNumber: number) {
  const httpServer = httpServerTable[httpServerId]

  httpServer.serverPortTable[portNumber] = httpServer.httpServer!.listen(portNumber)

  console.log(`httpServerId ${httpServerId} listening to portNumber: ${portNumber}`)
}

const startListeningToHttpServerRequests = {
  function: startListeningToHttpServerRequestsFunction
}

export {
  startListeningToHttpServerRequests
}
