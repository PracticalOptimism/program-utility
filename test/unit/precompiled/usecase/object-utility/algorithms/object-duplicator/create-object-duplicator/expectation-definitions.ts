
import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../@expectation-definitions/algorithms/get-project-file-path'

import { createObjectDuplicator } from '../create-object-duplicator'

describe(getProjectFilePath.function(__filename), () => {
  it('should create an object duplicator', () => {
    const originalObject = { name: 'Lucy' }
    const objectDuplicator = createObjectDuplicator.function(originalObject)
    expect(objectDuplicator.object).to.deep.equal(originalObject)
  })
})
