
import 'mocha'
import { getProjectFilePath } from '../../../../../@expectation-definitions/algorithms/get-project-file-path'
import { expect } from 'chai'
import { getObjectPropertyValueList } from './index'

describe(getProjectFilePath.function(__filename), () => {
  it('should return a list of all the object property values', () => {
    const objectPropertyValueList = getObjectPropertyValueList.function({
      property1: {
        property11: {
          property111: 'hello',
          property112: 'yes'
        },
        property12: {
          property121: 'hi',
          property122: 'hi'
        }
      },
      property2: {
        property21: 'hello',
        property22: 'hello'
      }
    }, { booleanObjectKeyNotAllowed: true, propertyKeyNameListOrderType: 'depthFirstOrder' })

    expect(objectPropertyValueList).to.deep.equal([
      'hello',
      'yes',
      'hi',
      'hi',
      'hello',
      'hello'
    ])
  })
  it('should return a list of the paired return properties objectPropertyKeyList and objectPropertyValue', () => {
    const objectPropertyValueList = getObjectPropertyValueList.function({
      property1: {
        property11: {
          property111: 'hello',
          property112: 'yes'
        },
        property12: {
          property121: 'hi',
          property122: 'hi'
        }
      },
      property2: {
        property21: 'hello',
        property22: 'hello'
      }
    }, { booleanObjectKeyNotAllowed: true, propertyKeyNameListOrderType: 'depthFirstOrder', returnObjectPropertyKeyListPair: true })

    expect(objectPropertyValueList).to.deep.equal([
      { objectPropertyKeyList: ['property1', 'property11', 'property111'], objectPropertyValue: 'hello' },
      { objectPropertyKeyList: ['property1', 'property11', 'property112'], objectPropertyValue: 'yes' },
      { objectPropertyKeyList: ['property1', 'property12', 'property121'], objectPropertyValue: 'hi' },
      { objectPropertyKeyList: ['property1', 'property12', 'property122'], objectPropertyValue: 'hi' },
      { objectPropertyKeyList: ['property2', 'property21'], objectPropertyValue: 'hello' },
      { objectPropertyKeyList: ['property2', 'property22'], objectPropertyValue: 'hello' }
    ])
  })
})

