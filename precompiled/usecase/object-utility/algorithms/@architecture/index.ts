
import * as object from '../object/@architecture'
import * as objectDuplicator from '../object-duplicator/@architecture'


export {
  object,
  objectDuplicator
}
