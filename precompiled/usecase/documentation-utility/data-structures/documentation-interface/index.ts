

class TableFormattedByHorizontalList {
  tableRowPropertyList: Array<{ rowPropertyId: string, columnPropertyId: string }> = []
  tableColumnPropertyList: Array<any> = []
  constructor (documentationInterfaceTable: TableFormattedByHorizontalList) {
    Object.assign(this, documentationInterfaceTable)
  }
}

class TableFormattedByVerticalList {
  tableColumnPropertyList: Array<{ columnPropertyId: string, columnListTableId: string }> = []
  tableColumnListTable: { [columnListTableId: string]: any[] } = {}
  constructor (documentationInterfaceTable: TableFormattedByVerticalList) {
    Object.assign(this, documentationInterfaceTable)
  }
}

export {
  TableFormattedByHorizontalList,
  TableFormattedByVerticalList
}

