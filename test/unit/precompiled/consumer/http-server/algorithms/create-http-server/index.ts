
import * as express from 'express'

import { httpServerTable } from '../../variables/http-server'
import { HttpServer } from '../../data-structures/http-server'

function createHttpServerFunction (): HttpServer {

  const httpServer = new HttpServer()
  httpServer.httpServerId = Math.random().toString()
  httpServer.httpServer = express()

  httpServer.httpServer.use(express.urlencoded({ extended: false }))
  httpServer.httpServer.use(express.json())

  httpServerTable[httpServer.httpServerId] = httpServer

  return httpServer
}

const createHttpServer = {
  function: createHttpServerFunction
}

export {
  createHttpServer
}

