
function getObjectValueFunction (object: any, keyList: string[]): any {
  if (!object) {
    return undefined
  }
  if (keyList.length === 0) {
    return object
  }
  if (!object[keyList[0]]) {
    return undefined
  }
  if (keyList.length === 1) {
    return object[keyList[0]]
  }
  return getObjectValueFunction(object[keyList[0]], keyList.slice(1))
}

const getObjectValue = {
  function: getObjectValueFunction
}

export {
  getObjectValue
}
