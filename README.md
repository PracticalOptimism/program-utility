
# Program Utility

### 
A few algorithms and data structures for accessing property values
of objects by using a property key name list. Also available,
you may create a deep copy of objects and/or track the copies
by using the ObjectDuplicator data structure.


##### Website Resources
[Source Code](https://gitlab.com/practicaloptimism/program-utility)&nbsp;| [Demos](https://practicaloptimism.gitlab.io/program-utility)&nbsp;| [Video Introduction](https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g)&nbsp;| [Video Tutorial](https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g)&nbsp;| [Live Programming Development Journal](https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g)

##### Project Development Status Updates
[![npm version](https://badge.fury.io/js/%40practicaloptimism%2Fprogram-utility.svg)](https://www.npmjs.com/package/%40practicaloptimism%2Fprogram-utility)&nbsp;[![downloads](https://img.shields.io/npm/dt/@practicaloptimism/program-utility)](https://www.npmjs.com/package/%40practicaloptimism%2Fprogram-utility)&nbsp;[![coverage report](https://gitlab.com/practicaloptimism/program-utility/badges/master/coverage.svg)](https://gitlab.com/practicaloptimism/program-utility/-/commits/master)&nbsp;[![pipeline status](https://gitlab.com/practicaloptimism/program-utility/badges/master/pipeline.svg)](https://gitlab.com/practicaloptimism/program-utility/-/commits/master)

##### This Documentation Page was last Updated on Mon Jan 24 2022 15:19:13 GMT-0600 (Central Standard Time)

### Installation

##### Node Package Manager (NPM) Installation
```
npm install --save @practicaloptimism/program-utility
```

##### Script import from JavaScript (requires NPM installation)
```javascript
import * as programUtility from '@practicaloptimism/program-utility'
```

##### HTML Script Import
```html
<script src="https://unpkg.com/@practicaloptimism/program-utility"></script>
```

### Getting Started: Accessing Object Property Value

```javascript
// Initialize the javascript library instance and the usecase property value
const programUtilityInstance = programUtility.algorithms.createJavascriptLibraryInstance.function()
const objectUtility =  programUtilityInstance.usecase.objectUtility.algorithms.object

// Create your javascript object
const object = { name: 'Hitomi', hobbyTable: { dancing: true, watchingScaryMovies: false } }

// Create your property value key list
const propertyKeyList = ['hobbyTable', 'dancing']

// Get your object property value using a key list
const propertyValue = objectUtility.getObjectValue.function(object, propertyKeyList)

// The value printed should return 'true'
console.log(propertyValue)
```

### Getting Started: Creating Object Duplicators

```javascript
// Initialize the javascript library instance and the usecase property value
const programUtilityInstance = programUtility.algorithms.createJavascriptLibraryInstance.function()
const  objectDuplicatorUtility =  programUtilityInstance.usecase.objectUtility.algorithms.objectDuplicator

// Create your javascript object
const object = { name: 'Miranda', hobbyTable: { watchingScaryMovies: true, watchingDanceRecitals: false } }

// Create your ObjectDuplicator data structure
const objectDuplicator = objectDuplicatorUtility.createObjectDuplicator.function(object)

// Add a duplicate of your object
objectDuplicatorUtility.addObjectToObjectDuplicator.function(objectDuplicator, 'item1')

// Get the duplicate of your object by using a sting value (objectDuplicateId)
const objectDuplicate = objectDuplicatorUtility.getObjectFromObjectDuplicator.function('item1')

// Change the 'name' property value of the object duplicate
// to showcase that the object and object duplicate
// will have different names since they are deep copies
// of one another.
objectDuplicate.name = 'Susan'

// The value printed should return 'false'
console.log(object.name === objectDuplicate.name)

```

### Application Programmable Interface (API Reference)


<pre><details open><summary>📁 API Documentation Reference (Click to Open)</summary>


<pre><details><summary>📁 @Project</summary><h5>@Project</h5>

<pre><details><summary>📁 Constants</summary><h5>Constants</h5>

<pre><details><summary>📁 Project</summary><h5>Project</h5>
</details></pre>
</details></pre>

<pre><details><summary>📁 DataStructures</summary><h5>DataStructures</h5>

<pre><details><summary>📁 Extract</summary><h5>Extract</h5><h5>PROJECT_DOCUMENTATION_FILE_PATH_DIRECTORY_NAME</h5>precompiled/@project/data-structures/extract

<pre><details><summary>📁 PROJECT_DATA_STRUCTURE_NAME_LIST</summary><h5>PROJECT_DATA_STRUCTURE_NAME_LIST</h5><h5>0</h5>ProjectExtract<h5>1</h5>ProjectUsecaseExtract
</details></pre><h5>PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</h5>undefined
</details></pre>
</details></pre>
</details></pre>

<pre><details><summary>📁 Consumer</summary><h5>Consumer</h5>

<pre><details><summary>📁 DocumentationInterface</summary><h5>DocumentationInterface</h5>
</details></pre>
</details></pre>

<pre><details><summary>📁 Usecase</summary><h5>Usecase</h5>

<pre><details><summary>📁 ObjectUtility</summary><h5>ObjectUtility</h5>

<pre><details><summary>📁 Algorithms</summary><h5>Algorithms</h5>

<pre><details><summary>📁 Object</summary><h5>Object</h5>

<pre><details><summary>📁 CreateObjectDeepCopy</summary><h5>CreateObjectDeepCopy</h5><h5>PROJECT_DOCUMENTATION_TEXT_HEADER</h5>createObjectDeepCopy<h5>PROJECT_DOCUMENTATION_TEXT_DESCRIPTION</h5>
This function copies an existing object and makes a copy
where the child objects don't reference one another in
memory.
<h5>PROJECT_DOCUMENTATION_FILE_PATH_DIRECTORY_NAME</h5>precompiled/usecase/object-utility/algorithms/object/create-object-deep-copy<h5>PROJECT_ALGORITHM_NAME</h5>getObjectValue

<pre><details><summary>📁 PROJECT_ALGORITHM_PARAMETER_LIST</summary><h5>PROJECT_ALGORITHM_PARAMETER_LIST</h5>

<pre><details><summary>📁 0</summary><h5>0</h5><h5>ParameterName</h5>object<h5>ParameterType</h5>any|Object

<pre><details><summary>📁 ParameterExampleList</summary><h5>ParameterExampleList</h5><h5>0</h5>{ "name": "Patrick Morrison", "address": { "city": "Beijing" } }
</details></pre>
</details></pre>

<pre><details><summary>📁 1</summary><h5>1</h5><h5>ParameterName</h5>propertyKeyNameList<h5>ParameterType</h5>string[]

<pre><details><summary>📁 ParameterExampleList</summary><h5>ParameterExampleList</h5><h5>0</h5>['address', 'city']
</details></pre>
</details></pre>
</details></pre>

<pre><details><summary>📁 PROJECT_ALGORITHM_RETURN_PARAMETER_LIST</summary><h5>PROJECT_ALGORITHM_RETURN_PARAMETER_LIST</h5>

<pre><details><summary>📁 0</summary><h5>0</h5><h5>ParameterName</h5>objectPropertyValue<h5>ParameterType</h5>any

<pre><details><summary>📁 ParameterExampleList</summary><h5>ParameterExampleList</h5><h5>0</h5>"Beijing"
</details></pre>
</details></pre>
</details></pre>

<pre><details><summary>📁 PROJECT_ALGORITHM_EXAMPLE_USECASE_LIST</summary><h5>PROJECT_ALGORITHM_EXAMPLE_USECASE_LIST</h5>

<pre><details><summary>📁 0</summary><h5>0</h5><h5>ProjectExampleUsecaseTitle</h5>Getting Started: Function call example usecase<h5>ProjectExampleUsecaseSourceCodeTextDescription</h5>getObjectValue.function(object, propertyKeyNameList)
</details></pre>

<pre><details><summary>📁 1</summary><h5>1</h5><h5>ProjectExampleUsecaseTitle</h5>Getting Started: Application context example usecase<h5>ProjectExampleTextDescription</h5>
  // Create a javascript library instance for your project
  const myProjectObjectUtility = objectUtility.consumer.javascript.algorithms.createJavascriptLibraryInstance.function('my-project-id')

  // Initialize a javascript object
  const myObject = { name: 'Claire', favoriteActivitiesTable: { dancing: { activityName: 'Dancing', placesVisitedToPerformActivity: ['New York', 'Los Angelos'], firstMemoryPerformingActivity: { placePerformedActivity: 'New York' } } }}

  // Retrieve the property value of myObject by using a property key name list
  const placePerformedActivityDuringFirstMemory = myProjectObjectUtility.usecase.objectUtility.algorithms.getObjectValue.function(myObject, ['favoriteActivitiesTable', 'dancing', 'firstMemoryPerformingActivity', 'placePerformedActivity'])

  // Print the place where the first memory of the dancing activity occurred as it is one of Claire's favorite activities
  console.log('The following should be "New York": ', placePerformedActivityDuringFirstMemory)
</details></pre>
</details></pre>

<pre><details><summary>📁 PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</summary><h5>PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</h5>

<pre><details><summary>📁 CreateObjectDeepCopy</summary><h5>CreateObjectDeepCopy</h5><h5>Function</h5>function createObjectDeepCopyFunction(object, option) {
    if (typeof object !== 'object') {
        return;
    }
    if (option && option.deepObjectClonePropertyTreeHeight === 0) {
        return object;
    }
    const newObject = {};
    let objectPropertyKeyNameList = [];
    const originalObjectPropertyKeyNameList = Object.keys(object).map(objectPropertyKeyName => [objectPropertyKeyName]);
    objectPropertyKeyNameList = objectPropertyKeyNameList.concat(originalObjectPropertyKeyNameList);
    for (let i = 0; i < objectPropertyKeyNameList.length; i++) {
        const propertyKeyNameList = objectPropertyKeyNameList[i];
        const objectPropertyValue = _get_object_value__WEBPACK_IMPORTED_MODULE_0__["getObjectValue"].function(object, propertyKeyNameList);
        if ((option && option.deepObjectClonePropertyTreeHeight !== undefined) &&
            ((propertyKeyNameList.length > option.deepObjectClonePropertyTreeHeight) ||
                ((typeof objectPropertyValue === 'object') && (propertyKeyNameList.length + 1 > option.deepObjectClonePropertyTreeHeight)))) {
            _update_object_value__WEBPACK_IMPORTED_MODULE_1__["updateObjectValue"].function(newObject, propertyKeyNameList, objectPropertyValue);
            continue;
        }
        if (typeof objectPropertyValue === 'object') {
            let childObjectPropertyKeyNameList;
            if (Array.isArray(objectPropertyValue)) {
                childObjectPropertyKeyNameList = objectPropertyValue.map((_objectPropertyKeyValue, index) => [...propertyKeyNameList, index]);
            }
            else {
                childObjectPropertyKeyNameList = Object.keys(objectPropertyValue).map(objectPropertyKeyName => [...propertyKeyNameList, objectPropertyKeyName]);
            }
            objectPropertyKeyNameList = objectPropertyKeyNameList.concat(childObjectPropertyKeyNameList);
        }
        else {
            _update_object_value__WEBPACK_IMPORTED_MODULE_1__["updateObjectValue"].function(newObject, propertyKeyNameList, objectPropertyValue);
        }
    }
    return newObject;
}
</details></pre>
</details></pre>
</details></pre>

<pre><details><summary>📁 GetObjectPropertyValueList</summary><h5>GetObjectPropertyValueList</h5><h5>PROJECT_DOCUMENTATION_TEXT_HEADER</h5>getObjectPropertyValueList

<pre><details><summary>📁 PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</summary><h5>PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</h5>

<pre><details><summary>📁 GetObjectPropertyValueList</summary><h5>GetObjectPropertyValueList</h5><h5>Function</h5>function getObjectPropertyValueListFunction(object, option) {
    const propertyKeyNameList = _get_object_property_key_name_list__WEBPACK_IMPORTED_MODULE_0__["getObjectPropertyKeyNameList"].function(object, [], option).map((keyList) => {
        const objectPropertyValue = _get_object_value__WEBPACK_IMPORTED_MODULE_1__["getObjectValue"].function(object, keyList);
        if (option && option.returnObjectPropertyKeyListPair) {
            return { objectPropertyKeyList: keyList, objectPropertyValue };
        }
        else {
            return objectPropertyValue;
        }
    });
    return propertyKeyNameList;
}
</details></pre><h5>FunctionOption</h5>class FunctionOption extends _get_object_property_key_name_list__WEBPACK_IMPORTED_MODULE_0__["FunctionOption"] {
    constructor(defaultOption) {
        super(defaultOption);
        Object.assign(this, defaultOption);
    }
}
</details></pre>
</details></pre>

<pre><details><summary>📁 DeleteObjectValue</summary><h5>DeleteObjectValue</h5><h5>PROJECT_DOCUMENTATION_TEXT_HEADER</h5>deleteObjectValue

<pre><details><summary>📁 PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</summary><h5>PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</h5>

<pre><details><summary>📁 DeleteObjectValue</summary><h5>DeleteObjectValue</h5><h5>Function</h5>function deleteObjectValueFunction(object, keyNameList, option) {
    _update_object_value__WEBPACK_IMPORTED_MODULE_0__["updateObjectValue"].function(object, keyNameList, undefined, option);
}
</details></pre><h5>FunctionOption</h5>class FunctionOption {
    constructor(defaultOption) {
        Object.assign(this, defaultOption);
    }
}
</details></pre>
</details></pre>

<pre><details><summary>📁 GetObjectPropertyKeyNameList</summary><h5>GetObjectPropertyKeyNameList</h5><h5>PROJECT_DOCUMENTATION_TEXT_HEADER</h5>getObjectPropertyKeyNameList

<pre><details><summary>📁 PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</summary><h5>PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</h5>

<pre><details><summary>📁 GetObjectPropertyKeyNameList</summary><h5>GetObjectPropertyKeyNameList</h5><h5>Function</h5>function getObjectPropertyKeyNameListFunction(object, parentKeyNameList, option) {
    if (option && option.propertyKeyNameListOrderType === 'depthFirstOrder') {
        return getObjectPropertyKeyNameListByDepthFirstOrderFunction(object, parentKeyNameList, option);
    }
    else {
        return getObjectPropertyKeyNameListByBreadthFirstOrderFunction(object, parentKeyNameList, option);
    }
}
</details></pre><h5>FunctionOption</h5>class FunctionOption {
    constructor(defaultOption) {
        Object.assign(this, defaultOption);
    }
}<h5>GetObjectPropertyKeyNameListByDepthFirstOrderFunction</h5>function getObjectPropertyKeyNameListByDepthFirstOrderFunction(object, parentKeyNameList, option) {
    if (!object) {
        return [];
    }
    if (typeof object !== 'object') {
        return [];
    }
    if (!parentKeyNameList || parentKeyNameList.length < 0) {
        parentKeyNameList = [];
    }
    let propertyKeyNameList = [];
    if ((parentKeyNameList.length > 0) && (option ? !option.booleanObjectKeyNotAllowed : true)) {
        propertyKeyNameList.push(parentKeyNameList);
    }
    for (let objectKey in object) {
        if (!object.hasOwnProperty(objectKey)) {
            continue;
        }
        const objectPropertyKeyNameList = [...parentKeyNameList, objectKey];
        if (option && (option.objectPropertyTreeHeight !== undefined) && (objectPropertyKeyNameList.length > option.objectPropertyTreeHeight)) {
            return [];
        }
        if (typeof object[objectKey] !== 'object') {
            propertyKeyNameList.push(objectPropertyKeyNameList);
            continue;
        }
        propertyKeyNameList.push(...getObjectPropertyKeyNameListByDepthFirstOrderFunction(object[objectKey], objectPropertyKeyNameList, option));
    }
    return propertyKeyNameList;
}<h5>GetObjectPropertyKeyNameListByBreadthFirstOrderFunction</h5>function getObjectPropertyKeyNameListByBreadthFirstOrderFunction(object, parentKeyNameList, option) {
    if (!object) {
        return [];
    }
    if (typeof object !== 'object') {
        return [];
    }
    if (!parentKeyNameList || parentKeyNameList.length < 0) {
        parentKeyNameList = [];
    }
    let propertyKeyNameList = Object.keys(object).map(objectKey => {
        const propertyKeyName = Array.isArray(object) ? parseInt(objectKey, 10) : objectKey;
        return parentKeyNameList.concat(propertyKeyName);
    });
    let removeObjectKeyNameListReferenceTable = {};
    if (option && (option.objectPropertyTreeHeight !== undefined) && (propertyKeyNameList[0].length > option.objectPropertyTreeHeight)) {
        return [];
    }
    for (let i = 0; i < propertyKeyNameList.length; i++) {
        const objectPropertyKeyNameList = propertyKeyNameList[i];
        const objectChildObjectValue = _get_object_value__WEBPACK_IMPORTED_MODULE_0__["getObjectValue"].function(object, objectPropertyKeyNameList);
        const objectChildPropertyKeyNameList = getObjectPropertyKeyNameListByBreadthFirstOrderFunction(objectChildObjectValue, objectPropertyKeyNameList, option);
        if (option && option.booleanObjectKeyNotAllowed && typeof objectChildObjectValue === 'object') {
            removeObjectKeyNameListReferenceTable[i] = true;
        }
        propertyKeyNameList.push(...objectChildPropertyKeyNameList);
    }
    const propertyKeyNameListWithoutObjectKey = [];
    if (option && option.booleanObjectKeyNotAllowed) {
        for (let i = 0; i < propertyKeyNameList.length; i++) {
            const removeKeyNameListIndex = removeObjectKeyNameListReferenceTable[i];
            if (removeKeyNameListIndex) {
                continue;
            }
            propertyKeyNameListWithoutObjectKey.push(propertyKeyNameList[i]);
        }
        propertyKeyNameList = propertyKeyNameListWithoutObjectKey;
    }
    return propertyKeyNameList;
}
</details></pre>
</details></pre>

<pre><details><summary>📁 GetObjectValue</summary><h5>GetObjectValue</h5><h5>PROJECT_DOCUMENTATION_TEXT_HEADER</h5>getObjectValue

<pre><details><summary>📁 PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</summary><h5>PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</h5>

<pre><details><summary>📁 GetObjectValue</summary><h5>GetObjectValue</h5><h5>Function</h5>function getObjectValueFunction(object, propertyKeyNameList) {
    if (!object) {
        return undefined;
    }
    if (propertyKeyNameList.length === 0) {
        return object;
    }
    if (!object[propertyKeyNameList[0]]) {
        return undefined;
    }
    if (propertyKeyNameList.length === 1) {
        return object[propertyKeyNameList[0]];
    }
    return getObjectValueFunction(object[propertyKeyNameList[0]], propertyKeyNameList.slice(1));
}
</details></pre>
</details></pre>
</details></pre>

<pre><details><summary>📁 IsObjectDeepEqual</summary><h5>IsObjectDeepEqual</h5><h5>PROJECT_DOCUMENTATION_TEXT_HEADER</h5>isObjectDeepEqual

<pre><details><summary>📁 PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</summary><h5>PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</h5>

<pre><details><summary>📁 IsObjectDeepEqual</summary><h5>IsObjectDeepEqual</h5><h5>Function</h5>function isObjectDeepEqualFunction(object1, object2) {
    if (typeof object1 !== 'object') {
        return false;
    }
    if (typeof object1 !== typeof object2) {
        return false;
    }
    if (((object1 instanceof Array) && !(object2 instanceof Array)) || (!(object1 instanceof Array) && (object2 instanceof Array))) {
        return false;
    }
    for (let propertyKeyName in object1) {
        if (!object1.hasOwnProperty(propertyKeyName)) {
            continue;
        }
        if (!object2.hasOwnProperty(propertyKeyName)) {
            return false;
        }
        if (typeof object1[propertyKeyName] === 'object') {
            continue;
        }
        if (object1[propertyKeyName] !== object2[propertyKeyName]) {
            return false;
        }
    }
    for (let propertyKeyName in object2) {
        if (!object2.hasOwnProperty(propertyKeyName)) {
            continue;
        }
        if (!object1.hasOwnProperty(propertyKeyName)) {
            return false;
        }
        if (typeof object2[propertyKeyName] === 'object') {
            continue;
        }
        if (object2[propertyKeyName] !== object2[propertyKeyName]) {
            return false;
        }
    }
    return true;
}
</details></pre>
</details></pre>
</details></pre>

<pre><details><summary>📁 UpdateObjectValue</summary><h5>UpdateObjectValue</h5><h5>PROJECT_DOCUMENTATION_TEXT_HEADER</h5>updateObjectValue

<pre><details><summary>📁 PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</summary><h5>PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</h5>

<pre><details><summary>📁 UpdateObjectValue</summary><h5>UpdateObjectValue</h5><h5>Function</h5>function updateObjectValueFunction(object, keyNameList, value, option) {
    if (!object) {
        if (typeof keyNameList[0] === 'number') {
            object = (new Array(keyNameList[0] + 1)).fill(undefined);
        }
        else {
            object = {};
        }
    }
    if (keyNameList.length === 0) {
        return object;
    }
    let objectValue = value;
    if (keyNameList.length !== 1) {
        objectValue = updateObjectValueFunction(object[keyNameList[0]], keyNameList.slice(1), value, option);
    }
    if (typeof object === 'object') {
        object[keyNameList[0]] = objectValue;
    }
    else if (typeof object === 'string') {
        const updateIndex = keyNameList[0];
        if (option && option.replaceStringObjectAtKeyIndex) {
            object = [object.slice(0, updateIndex), objectValue, object.slice(updateIndex + objectValue.length)].join('');
        }
        else {
            object = object.slice(0, updateIndex) + objectValue + object.slice(updateIndex);
        }
    }
    if ((option && option.deletePropertyKey) && (objectValue === undefined)) {
        delete object[keyNameList[0]];
    }
    return object;
}
</details></pre><h5>FunctionOption</h5>class FunctionOption {
    constructor(defaultOption) {
        Object.assign(this, defaultOption);
    }
}
</details></pre>
</details></pre>

<pre><details><summary>📁 UpdateObjectValueByMapFunction</summary><h5>UpdateObjectValueByMapFunction</h5><h5>PROJECT_DOCUMENTATION_TEXT_HEADER</h5>updateObjectValueByMapFunction

<pre><details><summary>📁 PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</summary><h5>PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</h5>

<pre><details><summary>📁 UpdateObjectValueByMapFunction</summary><h5>UpdateObjectValueByMapFunction</h5><h5>Function</h5>function updateObjectValueByMapFunctionFunction(object, mapFunction, option) {
    if (option && option.shouldCreateDeepObjectClone) {
        object = _create_object_deep_copy__WEBPACK_IMPORTED_MODULE_0__["createObjectDeepCopy"].function(object, option);
    }
    const objectMapResultList = _get_object_property_key_name_list__WEBPACK_IMPORTED_MODULE_2__["getObjectPropertyKeyNameList"].function(object, [], option).map((originalObjectPropertyKeyList, listIndex) => {
        let objectPropertyKeyList = originalObjectPropertyKeyList;
        let objectPropertyValue;
        const objectMapResult = mapFunction(objectPropertyKeyList, _get_object_value__WEBPACK_IMPORTED_MODULE_1__["getObjectValue"].function(object, objectPropertyKeyList), listIndex);
        if (typeof objectMapResult === 'object' && (objectMapResult.objectPropertyKey || objectMapResult.objectPropertyValue)) {
            if (objectPropertyKeyList !== objectMapResult.propertyKey && objectMapResult.propertyKey !== '') {
                _delete_object_value__WEBPACK_IMPORTED_MODULE_3__["deleteObjectValue"].function(object, objectPropertyKeyList);
            }
            objectPropertyKeyList = objectMapResult.objectPropertyKeyList || objectPropertyKeyList;
            objectPropertyValue = objectMapResult.objectPropertyValue;
        }
        else {
            objectPropertyValue = objectMapResult;
        }
        return { objectPropertyKeyList, objectPropertyValue };
    });
    for (let objectMapResultListIndex = 0; objectMapResultListIndex < objectMapResultList.length; objectMapResultListIndex++) {
        _update_object_value__WEBPACK_IMPORTED_MODULE_4__["updateObjectValue"].function(object, objectMapResultList[objectMapResultListIndex].objectPropertyKeyList, objectMapResultList[objectMapResultListIndex].objectPropertyValue);
    }
    return object;
}
</details></pre>
</details></pre>
</details></pre>
</details></pre>

<pre><details><summary>📁 ObjectDuplicator</summary><h5>ObjectDuplicator</h5>

<pre><details><summary>📁 CreateDuplicate</summary><h5>CreateDuplicate</h5><h5>PROJECT_DOCUMENTATION_TEXT_HEADER</h5>createDuplicate

<pre><details><summary>📁 PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</summary><h5>PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</h5>

<pre><details><summary>📁 CreateDuplicate</summary><h5>CreateDuplicate</h5><h5>Function</h5>function createDuplicateFunction(objectDuplicator, duplicateId) {
    objectDuplicator.objectDuplicateTable[duplicateId] = _object_create_object_deep_copy__WEBPACK_IMPORTED_MODULE_0__["createObjectDeepCopy"].function(objectDuplicator.object);
    return objectDuplicator.objectDuplicateTable[duplicateId];
}
</details></pre>
</details></pre>
</details></pre>

<pre><details><summary>📁 CreateObjectDuplicator</summary><h5>CreateObjectDuplicator</h5><h5>PROJECT_DOCUMENTATION_TEXT_HEADER</h5>createObjectDuplicator

<pre><details><summary>📁 PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</summary><h5>PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</h5>

<pre><details><summary>📁 CreateObjectDuplicator</summary><h5>CreateObjectDuplicator</h5><h5>Function</h5>function createObjectDuplicatorFunction(object) {
    return new _data_structures_object_duplicator__WEBPACK_IMPORTED_MODULE_0__["ObjectDuplicator"](_object_create_object_deep_copy__WEBPACK_IMPORTED_MODULE_1__["createObjectDeepCopy"].function(object));
}
</details></pre>
</details></pre>
</details></pre>

<pre><details><summary>📁 GetDuplicate</summary><h5>GetDuplicate</h5><h5>PROJECT_DOCUMENTATION_TEXT_HEADER</h5>getDuplicate

<pre><details><summary>📁 PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</summary><h5>PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</h5>

<pre><details><summary>📁 GetDuplicate</summary><h5>GetDuplicate</h5><h5>Function</h5>function getDuplicateFunction(objectDuplicator, duplicateId, option) {
    let object = objectDuplicator.objectDuplicateTable[duplicateId];
    if (object === undefined && option && option.booleanAddObjectIfUndefined) {
        object = _create_duplicate__WEBPACK_IMPORTED_MODULE_0__["createDuplicate"].function(objectDuplicator, duplicateId);
    }
    return object;
}
</details></pre>
</details></pre>
</details></pre>

<pre><details><summary>📁 RemoveDuplicate</summary><h5>RemoveDuplicate</h5><h5>PROJECT_DOCUMENTATION_TEXT_HEADER</h5>deleteDuplicate

<pre><details><summary>📁 PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</summary><h5>PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</h5>

<pre><details><summary>📁 DeleteDuplicate</summary><h5>DeleteDuplicate</h5><h5>Function</h5>function deleteDuplicateFunction(objectDuplicator, duplicateId) {
    delete objectDuplicator.objectDuplicateTable[duplicateId];
}
</details></pre>
</details></pre>
</details></pre>
</details></pre>
</details></pre>

<pre><details><summary>📁 DataStructures</summary><h5>DataStructures</h5>

<pre><details><summary>📁 ObjectDuplicator</summary><h5>ObjectDuplicator</h5><h5>PROJECT_DOCUMENTATION_FILE_PATH_DIRECTORY_NAME</h5>precompiled/usecase/object-utility/algorithms/object/create-object-deep-copy<h5>PROJECT_ALGORITHM_NAME</h5>getObjectValue

<pre><details><summary>📁 PROJECT_ALGORITHM_PARAMETER_LIST</summary><h5>PROJECT_ALGORITHM_PARAMETER_LIST</h5>

<pre><details><summary>📁 0</summary><h5>0</h5><h5>ParameterName</h5>object<h5>ParameterType</h5>any|Object

<pre><details><summary>📁 ParameterExampleList</summary><h5>ParameterExampleList</h5><h5>0</h5>{ "name": "Patrick Morrison", "address": { "city": "Beijing" } }
</details></pre>
</details></pre>

<pre><details><summary>📁 1</summary><h5>1</h5><h5>ParameterName</h5>propertyKeyNameList<h5>ParameterType</h5>string[]

<pre><details><summary>📁 ParameterExampleList</summary><h5>ParameterExampleList</h5><h5>0</h5>['address', 'city']
</details></pre>
</details></pre>
</details></pre>

<pre><details><summary>📁 PROJECT_ALGORITHM_RETURN_PARAMETER_LIST</summary><h5>PROJECT_ALGORITHM_RETURN_PARAMETER_LIST</h5>

<pre><details><summary>📁 0</summary><h5>0</h5><h5>ParameterName</h5>objectPropertyValue<h5>ParameterType</h5>any

<pre><details><summary>📁 ParameterExampleList</summary><h5>ParameterExampleList</h5><h5>0</h5>"Beijing"
</details></pre>
</details></pre>
</details></pre>

<pre><details><summary>📁 PROJECT_ALGORITHM_EXAMPLE_USECASE_LIST</summary><h5>PROJECT_ALGORITHM_EXAMPLE_USECASE_LIST</h5>

<pre><details><summary>📁 0</summary><h5>0</h5><h5>ProjectExampleUsecaseTitle</h5>Getting Started: Function call example usecase<h5>ProjectExampleUsecaseSourceCodeTextDescription</h5>getObjectValue.function(object, propertyKeyNameList)
</details></pre>

<pre><details><summary>📁 1</summary><h5>1</h5><h5>ProjectExampleUsecaseTitle</h5>Getting Started: Application context example usecase<h5>ProjectExampleTextDescription</h5>
  // Create a javascript library instance for your project
  const myProjectObjectUtility = objectUtility.consumer.javascript.algorithms.createJavascriptLibraryInstance.function('my-project-id')

  // Initialize a javascript object
  const myObject = { name: 'Claire', favoriteActivitiesTable: { dancing: { activityName: 'Dancing', placesVisitedToPerformActivity: ['New York', 'Los Angelos'], firstMemoryPerformingActivity: { placePerformedActivity: 'New York' } } }}

  // Retrieve the property value of myObject by using a property key name list
  const placePerformedActivityDuringFirstMemory = myProjectObjectUtility.usecase.objectUtility.algorithms.getObjectValue.function(myObject, ['favoriteActivitiesTable', 'dancing', 'firstMemoryPerformingActivity', 'placePerformedActivity'])

  // Print the place where the first memory of the dancing activity occurred as it is one of Claire's favorite activities
  console.log('The following should be "New York": ', placePerformedActivityDuringFirstMemory)
</details></pre>
</details></pre>

<pre><details><summary>📁 PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</summary><h5>PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT</h5><h5>ObjectDuplicator</h5>class ObjectDuplicator {
    constructor(object) {
        this.objectDuplicateTable = {};
        this.object = object;
    }
}
</details></pre>
</details></pre>
</details></pre>
</details></pre>

<pre><details><summary>📁 DocumentationUtility</summary><h5>DocumentationUtility</h5>

<pre><details><summary>📁 Algorithms</summary><h5>Algorithms</h5>
</details></pre>

<pre><details><summary>📁 DataStructures</summary><h5>DataStructures</h5>

<pre><details><summary>📁 DocumentationInterface</summary><h5>DocumentationInterface</h5>
</details></pre>
</details></pre>
</details></pre>
</details></pre>

<pre><details><summary>📁 Provider</summary><h5>Provider</h5>
</details></pre>
</details></pre>

### Operating Environment: JavaScript Runtime Environments

| JavaScript Runtime Environment | Node.js | Node.js Worker Thread | Web Worker | Google Chrome | Mozilla Firefox | Apple Safari | Beaker Browser |
| -- | -- | -- | -- | -- | -- | -- | -- |
| Supported Versions of the Runtime | The latest version(s) | The latest version(s) | The latest version(s) | The latest version(s) | The latest version(s) | The latest version(s) | The latest version(s) |

### To Resolve Problems and Submit Feature Requests

To report issues or submit feature requests with this projectConstants, please visit [Program Utility Issue Tracker](https://gitlab.com/practicaloptimism/program-utility/issues)

### About This Project


### Benefits


### Features


### Limitations
- 🤓 **Work-in-progress**: This project is a work-in-progress.
  The project architecture and documentation are being updated
  regularly. Please learn more about the development life cycle
  by visiting our **live programming development sessions on youtube**:
  [https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g](https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g)

### Related Work

There are many projects relating to the application usecases that Program Utility
strives to provide. A "**project usecase 1**", a "**project usecase 2**", a "**project usecase 3**"
are the primary goal for Program Utility. This is a non-exhaustive list of other projects in the world
that are being used and also relate to Program Utility usecase areas-of-interest
(well renowned projects are prioritized in our listing order strategy):


| Object Utility Providers |
| -- |
| -- |

### Acknowledgements

### License
MIT

