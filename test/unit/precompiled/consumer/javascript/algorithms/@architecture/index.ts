
import { addMainJavascriptLibraryToGlobalEnvironment } from '../add-main-javascript-library-to-global-environment'
import { createJavascriptLibraryInstance } from '../create-javascript-library-instance'

export {
  addMainJavascriptLibraryToGlobalEnvironment,
  createJavascriptLibraryInstance
}

