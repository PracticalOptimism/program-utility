

import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../@expectation-definitions/algorithms/get-project-file-path'
import { deleteObjectValue } from './index'

describe(getProjectFilePath.function(__filename), () => {
  it('should remove a property value from an object without removing the property key', () => {
    const myObject = { name: 'Claire', favoriteActivitiesTable: { dancing: { activityName: 'Dancing', placesVisitedToPerformActivity: ['New York', 'Los Angelos'], firstMemoryPerformingActivity: { placePerformedActivity: 'New York' } } } }
    deleteObjectValue.function(myObject, ['favoriteActivitiesTable', 'dancing', 'firstMemoryPerformingActivity', 'placePerformedActivity'], { deletePropertyKey: false })
    expect(myObject.favoriteActivitiesTable.dancing.firstMemoryPerformingActivity.placePerformedActivity).to.equal(undefined)
    expect(Object.keys(myObject.favoriteActivitiesTable.dancing.firstMemoryPerformingActivity).length).to.be.greaterThan(0)
  })
  it('should remove both the property value and key from an object', () => {
    const myObject = { name: 'Claire', favoriteActivitiesTable: { dancing: { activityName: 'Dancing', placesVisitedToPerformActivity: ['New York', 'Los Angelos'], firstMemoryPerformingActivity: { placePerformedActivity: 'New York' } } } }
    deleteObjectValue.function(myObject, ['favoriteActivitiesTable', 'dancing', 'firstMemoryPerformingActivity', 'placePerformedActivity'], { deletePropertyKey: true })
    expect(myObject.favoriteActivitiesTable.dancing.firstMemoryPerformingActivity.placePerformedActivity).to.equal(undefined)
    expect(Object.keys(myObject.favoriteActivitiesTable.dancing.firstMemoryPerformingActivity).length).to.be.equal(0)
  })
})

