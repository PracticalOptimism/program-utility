

import { createDuplicate } from '../create-duplicate'
import { createObjectDuplicator } from '../create-object-duplicator'
import { getDuplicate } from '../get-duplicate'
import { deleteDuplicate } from '../delete-duplicate'

export {
  createDuplicate,
  createObjectDuplicator,
  getDuplicate,
  deleteDuplicate
}
