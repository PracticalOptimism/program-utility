
import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../@expectation-definitions/algorithms/get-project-file-path'

import { createMarkdownTableFromJavascriptObject } from './index'
import { TableFormattedByHorizontalList, TableFormattedByVerticalList } from '../../../data-structures/documentation-interface'

describe(getProjectFilePath.function(__filename), () => {
  it('should return a horizontal list formatted table in markdown format', () => {
    const horizontalLisftFormattedTable = new TableFormattedByHorizontalList({
      tableRowPropertyList: [{
        rowPropertyId: 'Important Historical Names',
        columnPropertyId: 'name'
      }, {
        rowPropertyId: 'Occupations',
        columnPropertyId: 'occupation'
      }, {
        rowPropertyId: 'Occupation Date Occupancy Range List',
        columnPropertyId: 'occupationDateOccupancyRangeList'
      }],
      tableColumnPropertyList: [{
        name: 'Shinzō Abe',
        occupation: 'Prime Minister of Japan',
        occupationDateOccupancyRangeList: [
          { startDate: '2012', endDate: 'Present (Ongoing as of June 2020 in present recitation of this program)' }
        ]
      }, {
        name: 'Thomas Jefferson',
        occupation: 'United States President',
        occupationDateOccupancyRangeList: [
          { startDate: '1801', endDate: '1809' }
        ]
      }, {
        name: 'James Monroe',
        occupation: 'United States President',
        occupationDateOccupancyRangeList: [
          { startDate: '1817', endDate: '1825' }
        ]
      }, {
        name: 'Vladimir Putin',
        occupation: 'President of Russia',
        occupationDateOccupancyRangeList: [
          { startDate: '1999', endDate: '2008' },
          { startDate: '2012', endDate: 'Present (Ongoing as of June 2020 in present recitation of this program)' }
        ]
      }, {
        name: 'Li Keqiang',
        occupation: 'Premier of the State Council of the People\'s Republic of China',
        occupationDateOccupancyRangeList: [
          { startDate: '2013', endDate: 'Present (Ongoing as of June 2020 in present recitation of this program)' }
        ]
      }, {
        name: 'Elon Musk',
        occupation: 'President of Earth',
        occupationDateOccupancyRangeList: [
          { startDate: '20?? by unanimous vote by existing political leaders and world population vote', endDate: 'Not sure' }
        ]
      }]
    })
    const markdownTableString = createMarkdownTableFromJavascriptObject.function(horizontalLisftFormattedTable)

    expect(markdownTableString).to.contain('|')
  })
  it('should return a vertically formatted table in markdown format', () => {
    const horizontalLisftFormattedTable = new TableFormattedByVerticalList({
      tableColumnPropertyList: [{
        columnPropertyId: 'Something Important',
        columnListTableId: 'somethingImportant'
      }, {
        columnPropertyId: 'Another Important Thing',
        columnListTableId: 'anotherImportantThing'
      }],
      tableColumnListTable: {
        somethingImportant: ['something1', 'something2', 'something3'],
        anotherImportantThing: ['anotherthing1', 'anotherthing2', 'anotherthing3']
      }
    })
    const markdownTableString = createMarkdownTableFromJavascriptObject.function(horizontalLisftFormattedTable)

    expect(markdownTableString).to.contain('|')
  })
})

