

import { createHttpServer } from './algorithms/create-http-server'
import { createHttpServerRequestHandler } from './algorithms/create-http-server-request-handler'
import { createHttpServerRequestHandlerListByObject } from './algorithms/create-http-server-request-handler-list-by-object'
import { deleteHttpServer } from './algorithms/delete-http-server'
import { getHttpServer } from './algorithms/get-http-server'
import { startListeningToHttpServerRequests } from './algorithms/start-listening-to-http-server-requests'
import { stopListeningToHttpServerRequests } from './algorithms/stop-listening-to-http-server-requests'

import * as variablesModule from './variables/http-server'


const algorithms = {
  createHttpServer,
  createHttpServerRequestHandler,
  createHttpServerRequestHandlerListByObject,
  deleteHttpServer,
  getHttpServer,
  startListeningToHttpServerRequests,
  stopListeningToHttpServerRequests
}

const variables = {
  ...variablesModule
}

const dataStructures = {}

const constants = {}


export {
  algorithms,
  variables,
  dataStructures,
  constants
}


