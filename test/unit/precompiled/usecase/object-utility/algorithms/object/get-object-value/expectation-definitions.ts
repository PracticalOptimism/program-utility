
import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../@expectation-definitions/algorithms/get-project-file-path'

import { getObjectValue } from './index'


describe(getProjectFilePath.function(__filename), () => {
  it('should get the value of the object property at the specified propertyKeyNameList location', () => {
    const myObject = { name: 'Claire', favoriteActivitiesTable: { dancing: { activityName: 'Dancing', placesVisitedToPerformActivity: ['New York', 'Los Angelos'], firstMemoryPerformingActivity: { placePerformedActivity: 'New York' } } } }
    const value = getObjectValue.function(myObject, ['favoriteActivitiesTable', 'dancing', 'firstMemoryPerformingActivity', 'placePerformedActivity'])
    expect(value).to.equal('New York')
  })
})

