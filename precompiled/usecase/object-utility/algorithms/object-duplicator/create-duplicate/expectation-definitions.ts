
import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../@expectation-definitions/algorithms/get-project-file-path'

import { createObjectDuplicator } from '../create-object-duplicator'
import { createDuplicate } from './index'

describe(getProjectFilePath.function(__filename), () => {
  it('should create a deep copy of the source object', () => {
    const originalObject = { name: 'Lucy' }
    const objectDuplicator = createObjectDuplicator.function(originalObject)
    const objectDuplicate = createDuplicate.function(objectDuplicator, 'lucy1')
    expect(objectDuplicate).to.deep.equal(originalObject)
    expect(objectDuplicator.objectDuplicateTable['lucy1']).to.not.equal(null || undefined)
  })
})

