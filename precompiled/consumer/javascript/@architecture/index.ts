
import * as algorithms from '../algorithms/@architecture'
import * as constants from '../constants/@architecture'
import * as variables from '../variables/@architecture'

// This is used to prevent importing algorithms in the
// variable files which may otherwise result in circular
// imports. Circular imports aren't supported by webpack
// at the moment this comment was made. Webpack is a
// tool that helps make this project work, at this time.
variables.javascript.mainJavascriptLibrary.consumer.javascript = {
  algorithms,
  constants,
  variables
}

export {
  algorithms,
  constants,
  variables
}
