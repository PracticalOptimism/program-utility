
import * as objectDocumentation from '../object/@architecture/documentation'
import * as objectDuplicatorDocumentation from '../object-duplicator/@architecture/documentation'

export {
  objectDocumentation,
  objectDuplicatorDocumentation
}
