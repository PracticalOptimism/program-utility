


// 'data-structures/demos/import-with-typescript/index': './demos/data-structures/import-with-typescript/index.ts'


const webpackCommonConfig = require('./webpack.common.config')

module.exports = {
  ...webpackCommonConfig,
  target: 'node',
  mode: 'development',
  entry: {
    'data-structures/demos/import-with-typescript/index': './demos/data-structures/import-with-typescript/index.ts'
  },
  module: {
    rules: [{
      test: /\.tsx?$/,
      loader: 'ts-loader',
      exclude: /node_modules/,
      options: {
        compilerOptions: {
          declaration: false
        }
      }
    }]
  }
}


