
import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../@expectation-definitions/algorithms/get-project-file-path'

import { getObjectPropertyKeyNameList, getObjectPropertyKeyNameListByBreadthFirstOrderFunction } from './index'

describe(getProjectFilePath.function(__filename), () => {
  it('should return a list of propertyKeyName lists for each property of the object', () => {
    const myObject = { englishName: 'Elizabeth', address: { city: 'Seoul, Korea' } }

    expect(getObjectPropertyKeyNameList.function(myObject)).to.deep.equal([
      ['englishName'],
      ['address'],
      ['address', 'city']
    ])
  })
  it('should return a list of propertyKeyName lists for a given tree height', () => {
    const myObject = { koreanName: 'Doona', address: { city: 'Seoul, Korea' }, favoriteActivitiesTable: { dancing: { placesVisitedToPerformActivityTable: { madridSpain: 'Madrid, Spain', tokyoJapan: 'Tokyo, Japan' } } } }

    expect(getObjectPropertyKeyNameList.function(myObject, [], { objectPropertyTreeHeight: 2 })).to.deep.equal([
      ['koreanName'],
      ['address'],
      ['favoriteActivitiesTable'],
      ['address', 'city'],
      ['favoriteActivitiesTable', 'dancing']
      // ['favoriteActivitiesTable', 'dancing', 'placesVisitedToPerformActivityTable'],
      // ['favoriteActivitiesTable', 'dancing', 'placesVisitedToPerformActivityTable', 'madridSpain'],
      // ['favoriteActivitiesTable', 'dancing', 'placesVisitedToPerformActivityTable', 'tokyoJapan']
    ])
  }),
  it('should return a depth first ordered list of propertyKeyName lists for the whole height of the tree', () => {
    const myObject = { name: 'Michael Jackson', address: { city: 'Gary, Indiana' }, favoriteActivitiesTable: { dancing: { placesVisitedToPerformActivityTable: { aPlaceWithNoName: 'A Place With No Name', thisPlaceHotel: 'This Place Hotel' } } } }

    expect(getObjectPropertyKeyNameList.function(myObject, [], { propertyKeyNameListOrderType: 'depthFirstOrder' })).to.deep.equal([
      ['name'],
      ['address'],
      ['address', 'city'],
      ['favoriteActivitiesTable'],
      ['favoriteActivitiesTable', 'dancing'],
      ['favoriteActivitiesTable', 'dancing', 'placesVisitedToPerformActivityTable'],
      ['favoriteActivitiesTable', 'dancing', 'placesVisitedToPerformActivityTable', 'aPlaceWithNoName'],
      ['favoriteActivitiesTable', 'dancing', 'placesVisitedToPerformActivityTable', 'thisPlaceHotel']
    ])
  })
  it('should return a list of propertyKeyName without object key list references', () => {
    const myObject = { name: 'Michael Jackson', address: { city: 'Gary, Indiana' }, favoriteActivitiesTable: { dancing: { placesVisitedToPerformActivityTable: { aPlaceWithNoName: 'A Place With No Name', thisPlaceHotel: 'This Place Hotel' } } } }

    const objectKeyNotAllowedList = getObjectPropertyKeyNameListByBreadthFirstOrderFunction(myObject, [], { booleanObjectKeyNotAllowed: true })

    expect(objectKeyNotAllowedList).to.deep.equal([
      ['name'],
      // ['address'],
      ['address', 'city'],
      // ['favoriteActivitiesTable'],
      // ['favoriteActivitiesTable', 'dancing'],
      // ['favoriteActivitiesTable', 'dancing', 'placesVisitedToPerformActivityTable'],
      ['favoriteActivitiesTable', 'dancing', 'placesVisitedToPerformActivityTable', 'aPlaceWithNoName'],
      ['favoriteActivitiesTable', 'dancing', 'placesVisitedToPerformActivityTable', 'thisPlaceHotel']
    ])
  })
  it('should return a list where the array index is also included', () => {
    const myObject = { name: 'Michael Jackson', address: { city: 'Gary, Indiana' }, favoriteActivitiesList: [{ activityName: 'dancing', placesVisitedToPerformActivityTable: { aPlaceWithNoName: 'A Place With No Name', thisPlaceHotel: 'This Place Hotel' } } ] }

    const objectPropertyKeyNameList = getObjectPropertyKeyNameListByBreadthFirstOrderFunction(myObject)

    expect(objectPropertyKeyNameList).to.deep.equal([
      ['name'],
      ['address'],
      ['favoriteActivitiesList'],
      ['address', 'city'],
      ['favoriteActivitiesList', 0],
      ['favoriteActivitiesList', 0, 'activityName'],
      ['favoriteActivitiesList', 0, 'placesVisitedToPerformActivityTable'],
      ['favoriteActivitiesList', 0, 'placesVisitedToPerformActivityTable', 'aPlaceWithNoName'],
      ['favoriteActivitiesList', 0, 'placesVisitedToPerformActivityTable', 'thisPlaceHotel']
    ])
  })
})

