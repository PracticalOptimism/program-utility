
import { infiniteLoopTable } from '../variables/infinite-loop'

import { stopInfiniteLoop } from './stop-infinite-loop'

function deleteInfiniteLoopFunction (infiniteLoopId: string): boolean {
  const infiniteLoop = infiniteLoopTable[infiniteLoopId]

  if (!infiniteLoop) {
    return false
  }

  stopInfiniteLoop.function(infiniteLoopId)

  delete infiniteLoopTable[infiniteLoopId]

  return true
}


const deleteInfiniteLoop = {
  function: deleteInfiniteLoopFunction
}

export {
  deleteInfiniteLoop
}
