
// Data Structures

class ObjectDuplicator<ObjectType> {
  object: ObjectType
  objectDuplicateTable: { [objectDuplicateId: string]: ObjectType } = {}
  constructor (object: ObjectType) {
    this.object = object
  }
}

export {
  ObjectDuplicator
}

