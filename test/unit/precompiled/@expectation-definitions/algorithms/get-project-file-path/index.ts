
// Algorithms

function getProjectFilePathFunction (filePathName: string) {
  const indexOfPrecompiledFileName = filePathName.indexOf('/precompiled')
  const indexOfExpectationDefinitionFileName = filePathName.indexOf('/expectation-definitions.ts')
  return filePathName.slice(indexOfPrecompiledFileName, indexOfExpectationDefinitionFileName)
}

// Constants

const getProjectFilePath = {
  function: getProjectFilePathFunction
}

export {
  getProjectFilePath
}
