
import * as express from 'express'

import { HttpServer, HttpServerRequestResponse } from '../../data-structures/http-server'
import { httpServerTable } from '../../variables/http-server'
import { getObjectValue } from '../get-object-value'
import * as httpStatus from 'http-status'

function createHttpServerRequestHandlerListByObjectFunction (httpServerId: string, prefixPathName: string, object: any): HttpServer {

  const httpServer = httpServerTable[httpServerId]

  prefixPathName = prefixPathName[0] === '/' ? prefixPathName : `/${prefixPathName}`

  const objectPropertyKeyListPathName: string = `${prefixPathName}/:objectPropertyKeyList`

  httpServer.httpServer!.get(prefixPathName, (_request: express.Request, response: express.Response) => {
    const requestResponse = new HttpServerRequestResponse()
    requestResponse.responseValue = object

    response.send(requestResponse)
  })

  httpServer.httpServer!.get(objectPropertyKeyListPathName, createResponseHandlerFromObject(object))

  return httpServer
}

function createResponseHandlerFromObject (object: any) {
  return async (request: express.Request, response: express.Response) => {
    const pathName = request.path.split('/').slice(2).join('')
    const requestResponse = new HttpServerRequestResponse()

    let objectPropertyKeyList = pathName.split('.')

    // Find the object value of the requested property key list
    const objectValue = getObjectValue.function(object, objectPropertyKeyList)

    if (!objectValue) {
      requestResponse.statusCode = httpStatus.NOT_FOUND
      requestResponse.statusCodeDescription = (httpStatus as any)[httpStatus.NOT_FOUND]
      requestResponse.hasErrorOccurred = true
      requestResponse.errorDescription = `Cannot find object value for objectPropertyKeyList: ${objectPropertyKeyList}`
      response.send(requestResponse)
      return
    }

    // Determine the query parameter list
    const queryParam = request.query

    // Determine the request body parameter list
    const requestBody = request.body

    console.log(queryParam)

    if (typeof objectValue === 'function') {
      console.log('This is a function: ', true)
      const functionInputTable: any = queryParam ? queryParam : requestBody
      const functionInputList = Object.keys(functionInputTable).map((inputName: string) => functionInputTable[inputName])

      try {
        requestResponse.responseValue = await objectValue(...functionInputList)
      } catch (err) {
        requestResponse.statusCode = httpStatus.BAD_REQUEST
        requestResponse.statusCodeDescription = (httpStatus as any)[httpStatus.BAD_REQUEST]
        requestResponse.hasErrorOccurred = true
        requestResponse.errorDescription = (err).message.toString()
      }
    } else {
      requestResponse.responseValue = objectValue
    }

    response.send(requestResponse)
  }
}

const createHttpServerRequestHandlerListByObject = {
  function: createHttpServerRequestHandlerListByObjectFunction
}

export {
  createHttpServerRequestHandlerListByObject
}
