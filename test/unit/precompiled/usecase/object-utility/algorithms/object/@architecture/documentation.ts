
import * as createObjectDeepCopyDocumentation from '../create-object-deep-copy/documentation'
import * as getObjectPropertyValueListDocumentation from '../get-object-property-value-list/documentation'
import * as deleteObjectValueDocumentation from '../delete-object-value/documentation'
import * as getObjectPropertyKeyNameListDocumentation from '../get-object-property-key-name-list/documentation'
import * as getObjectValueDocumentation from '../get-object-value/documentation'
import * as isObjectDeepEqualDocumentation from '../is-object-deep-equal/documentation'
import * as updateObjectValueDocumentation from '../update-object-value/documentation'
import * as updateObjectValueByMapFunctionDocumentation from '../update-object-value-by-map-function/documentation'

export {
  createObjectDeepCopyDocumentation,
  deleteObjectValueDocumentation,
  getObjectPropertyKeyNameListDocumentation,
  getObjectPropertyValueListDocumentation,
  getObjectValueDocumentation,
  isObjectDeepEqualDocumentation,
  updateObjectValueDocumentation,
  updateObjectValueByMapFunctionDocumentation
}

