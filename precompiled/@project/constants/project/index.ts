
import { TableFormattedByHorizontalList, TableFormattedByVerticalList } from '../../../usecase/documentation-utility/data-structures/documentation-interface'

const PROJECT_ID = 'program-utility'
const PROJECT_NAME = 'Program Utility'
const PROJECT_ORGANIZATION_ID = ''
const PROJECT_ORGANIZATION_NAME = ''
const PROJECT_NAME_IDENTIFIER_GROUP = {}
const PROJECT_NAME_WEBSITE_IDENTIFIER_GROUP = {}

const PROJECT_JAVASCRIPT_LIBRARY_ID = 'programUtility'
const PROJECT_TEXT_DESCRIPTION_GROUP = {}
const PROJECT_ENVIRONMENT_OPERATION_OPTIONS = {}

const PROJECT_AUTHOR_LIST = ['Jon Ide']
const PROJECT_SOURCE_CODE_URL = `https://gitlab.com/practicaloptimism/${PROJECT_ID}`
const PROJECT_DEMOS_OPTION_URL = `https://practicaloptimism.gitlab.io/${PROJECT_ID}`
const PROJECT_VIDEO_INTRODUCTION_URL = `https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g`
const PROJECT_VIDEO_TUTORIAL = `https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g`
const PROJECT_VIDEO_DEVELOPMENT_JOURNAL_URL = `https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g`

const PROJECT_TYPE_TEXT_DESCRIPTION = `
A few algorithms and data structures for accessing property values
of objects by using a property key name list. Also available,
you may create a deep copy of objects and/or track the copies
by using the ObjectDuplicator data structure.
`

const PROJECT_JAVASCRIPT_OBJECT_PACKAGE_DESCRIPTION = {
  npmPackageId: `@practicaloptimism/${PROJECT_ID}`,
  htmlScriptSrcId: `https://unpkg.com/@practicaloptimism/${PROJECT_ID}`
}

const PROJECT_DEVELOPMENT_STATUS_UPDATE_BADGE_OBJECT = {
  npmVersionBadgeUrl: `https://badge.fury.io/js/${encodeURIComponent(PROJECT_JAVASCRIPT_OBJECT_PACKAGE_DESCRIPTION.npmPackageId)}.svg`,
  npmVersionResourceUrl: `https://www.npmjs.com/package/${encodeURIComponent(PROJECT_JAVASCRIPT_OBJECT_PACKAGE_DESCRIPTION.npmPackageId)}`,

  downloadsBadgeUrl: `https://img.shields.io/npm/dt/${PROJECT_JAVASCRIPT_OBJECT_PACKAGE_DESCRIPTION.npmPackageId}`,
  downloadsResourceUrl: `https://www.npmjs.com/package/${encodeURIComponent(PROJECT_JAVASCRIPT_OBJECT_PACKAGE_DESCRIPTION.npmPackageId)}`,

  coverageReportBadgeUrl: `${PROJECT_SOURCE_CODE_URL}/badges/master/coverage.svg`,
  coverageReportResourceUrl: `${PROJECT_SOURCE_CODE_URL}/-/commits/master`,

  pipelineStatusBadgeUrl: `${PROJECT_SOURCE_CODE_URL}/badges/master/pipeline.svg`,
  pipelineStatusResourceUrl: `${PROJECT_SOURCE_CODE_URL}/-/commits/master`
}

const PROJECT_OPERATING_ENVIRONMENT_LIST = [{
  operatingEnvironmentTableName: 'Operating Environment: JavaScript Runtime Environments',
  operatingEnvironmentTableObject: new TableFormattedByHorizontalList({
    tableRowPropertyList: [{
      rowPropertyId: 'JavaScript Runtime Environment',
      columnPropertyId: 'operatingEnvironmentName'
    }, {
      rowPropertyId: 'Supported Versions of the Runtime',
      columnPropertyId: 'operatingEnvironmentVersionSupportList'
    }],
    tableColumnPropertyList: [{
      operatingEnvironmentName: 'Node.js',
      operatingEnvironmentVersionSupportList: ['The latest version(s)']
    }, {
      operatingEnvironmentName: 'Node.js Worker Thread',
      operatingEnvironmentVersionSupportList: ['The latest version(s)']
    }, {
      operatingEnvironmentName: 'Web Worker',
      operatingEnvironmentVersionSupportList: ['The latest version(s)']
    }, {
      operatingEnvironmentName: 'Google Chrome',
      operatingEnvironmentVersionSupportList: ['The latest version(s)']
    }, {
      operatingEnvironmentName: 'Mozilla Firefox',
      operatingEnvironmentVersionSupportList: ['The latest version(s)']
    }, {
      operatingEnvironmentName: 'Apple Safari',
      operatingEnvironmentVersionSupportList: ['The latest version(s)']
    }, {
      operatingEnvironmentName: 'Beaker Browser',
      operatingEnvironmentVersionSupportList: ['The latest version(s)']
    }]
  })
}]

const PROJECT_PROBLEM_RESOLUTION_URL = `${PROJECT_SOURCE_CODE_URL}/issues`

const PROJECT_TEXT_DESCRIPTION = ``

const PROJECT_BENEFIT_LIST: string[] = []

const PROJECT_FEATURE_LIST: string[] = []

const PROJECT_LIMITATION_LIST: string[] = [
  `🤓 **Work-in-progress**: This project is a work-in-progress.
  The project architecture and documentation are being updated
  regularly. Please learn more about the development life cycle
  by visiting our **live programming development sessions on youtube**:
  [${PROJECT_VIDEO_DEVELOPMENT_JOURNAL_URL}](${PROJECT_VIDEO_DEVELOPMENT_JOURNAL_URL})`
]

const PROJECT_RELATED_WORK_TEXT_DESCRIPTION = `
There are many projects relating to the application usecases that ${PROJECT_NAME}
strives to provide. A "**project usecase 1**", a "**project usecase 2**", a "**project usecase 3**"
are the primary goal for ${PROJECT_NAME}. This is a non-exhaustive list of other projects in the world
that are being used and also relate to ${PROJECT_NAME} usecase areas-of-interest
(well renowned projects are prioritized in our listing order strategy):
`

// const PROJECT_RELATED_WORK_TABLE: { [projectUsecaseId: string]: string[] } = {}

const PROJECT_RELATED_WORK_TABLE = {
  projectRelatedWorkTableObject: new TableFormattedByVerticalList({
    tableColumnPropertyList: [{
      columnPropertyId: 'Object Utility Providers',
      columnListTableId: 'objectUtilityProviderList'
    }],
    tableColumnListTable: {
      objectUtilityProviderList: [{
        providerName: `${PROJECT_ID}`,
        providerAuthorList: PROJECT_AUTHOR_LIST,
        providerResourceUrl: PROJECT_SOURCE_CODE_URL
      }]
    }
  })
}

export {
  PROJECT_ID,
  PROJECT_NAME,
  PROJECT_ORGANIZATION_ID,
  PROJECT_ORGANIZATION_NAME,
  PROJECT_NAME_IDENTIFIER_GROUP,
  PROJECT_NAME_WEBSITE_IDENTIFIER_GROUP,

  PROJECT_JAVASCRIPT_LIBRARY_ID,
  PROJECT_TEXT_DESCRIPTION_GROUP,
  PROJECT_ENVIRONMENT_OPERATION_OPTIONS,

  PROJECT_SOURCE_CODE_URL,
  PROJECT_DEMOS_OPTION_URL,
  PROJECT_VIDEO_INTRODUCTION_URL,
  PROJECT_VIDEO_TUTORIAL,
  PROJECT_VIDEO_DEVELOPMENT_JOURNAL_URL,

  PROJECT_TYPE_TEXT_DESCRIPTION,
  PROJECT_JAVASCRIPT_OBJECT_PACKAGE_DESCRIPTION,
  PROJECT_DEVELOPMENT_STATUS_UPDATE_BADGE_OBJECT,

  PROJECT_OPERATING_ENVIRONMENT_LIST,

  PROJECT_PROBLEM_RESOLUTION_URL,
  PROJECT_TEXT_DESCRIPTION,
  PROJECT_BENEFIT_LIST,
  PROJECT_FEATURE_LIST,
  PROJECT_LIMITATION_LIST,

  PROJECT_RELATED_WORK_TEXT_DESCRIPTION,
  PROJECT_RELATED_WORK_TABLE
}
