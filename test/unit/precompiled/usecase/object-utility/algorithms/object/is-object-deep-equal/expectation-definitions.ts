
import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../@expectation-definitions/algorithms/get-project-file-path'
import { isObjectDeepEqual } from './index'


describe(getProjectFilePath.function(__filename), () => {
  it('should check if object is deep equal to another object', () => {
    const myObject1 = { name: 'Claire', favoriteActivitiesTable: { dancing: { activityName: 'Dancing', placesVisitedToPerformActivity: ['New York', 'Los Angelos'], firstMemoryPerformingActivity: { placePerformedActivity: 'New York' } } } }
    const myObject2 = { name: 'Claire', favoriteActivitiesTable: { dancing: { activityName: 'Dancing', placesVisitedToPerformActivity: ['New York', 'Los Angelos'], firstMemoryPerformingActivity: { placePerformedActivity: 'New York' } } } }
    const isEqual = isObjectDeepEqual.function(myObject1, myObject2)
    expect(isEqual).to.equal(true)
  })
})
