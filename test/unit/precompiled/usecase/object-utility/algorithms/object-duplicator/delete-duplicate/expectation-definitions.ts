
import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../@expectation-definitions/algorithms/get-project-file-path'

import { createObjectDuplicator } from '../create-object-duplicator'
import { createDuplicate } from '../create-duplicate'
import { deleteDuplicate } from './index'

describe(getProjectFilePath.function(__filename), () => {
  it('should delete object duplicate from object duplicator', () => {
    const originalObject = { name: 'Lucy' }
    const objectDuplicator = createObjectDuplicator.function(originalObject)
    const objectDuplicate = createDuplicate.function(objectDuplicator, 'lucy1')

    // Expect lucy1 to be defined
    expect(objectDuplicate).to.deep.equal(originalObject)

    // Delete lucy1 from the object duplicator
    deleteDuplicate.function(objectDuplicator, 'lucy1')

    // Expect lucy1 to be null or undefined
    expect(objectDuplicator.objectDuplicateTable['lucy1']).to.equal(null || undefined)
  })
})

