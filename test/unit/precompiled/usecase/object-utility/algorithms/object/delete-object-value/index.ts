
import { updateObjectValue } from '../update-object-value'

// Data Structures

class FunctionOption {
  deletePropertyKey?: boolean
  originalObject?: any
  constructor (defaultOption?: FunctionOption) {
    Object.assign(this, defaultOption)
  }
}

// Algorithms

function deleteObjectValueFunction (object: any, keyNameList: (number | string)[], option?: FunctionOption): any {
  updateObjectValue.function(object, keyNameList, undefined, option)
}

// Constants

const deleteObjectValue = {
  function: deleteObjectValueFunction
}

export {
  deleteObjectValue,
  FunctionOption
}

