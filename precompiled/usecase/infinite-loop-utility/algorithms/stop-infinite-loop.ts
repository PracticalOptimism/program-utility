
import { infiniteLoopTable } from '../variables/infinite-loop'

function stopInfiniteLoopFunction (infiniteLoopId: string): boolean {
  const infiniteLoop = infiniteLoopTable[infiniteLoopId]

  if (!infiniteLoop) {
    return false
  }

  infiniteLoop.isStarted = false
  clearTimeout(infiniteLoop.setTimeoutId)
  return true
}

const stopInfiniteLoop = {
  function: stopInfiniteLoopFunction
}

export {
  stopInfiniteLoop
}
