
import { getObjectValue } from '../get-object-value'

// Data Structures

class FunctionOption {
  booleanObjectKeyNotAllowed?: boolean
  propertyKeyNameListOrderType?: 'breadthFirstOrder' | 'depthFirstOrder'
  objectPropertyTreeHeight?: number
  constructor (defaultOption?: FunctionOption) {
    Object.assign(this, defaultOption)
  }
}

// Algorithms

function getObjectPropertyKeyNameListFunction (object: any, parentKeyNameList?: (number | string)[], option?: FunctionOption): Array<(number | string)[]> {
  // Calculate depth first order traversal if specified by propertyKeyNameListOrderType
  if (option && option.propertyKeyNameListOrderType === 'depthFirstOrder') {
    return getObjectPropertyKeyNameListByDepthFirstOrderFunction(object, parentKeyNameList, option)
  // Follow breadth first order traversal by default or if specified by propertyKeyNameListOrderType
  } else {
    return getObjectPropertyKeyNameListByBreadthFirstOrderFunction(object, parentKeyNameList, option)
  }
}

function getObjectPropertyKeyNameListByDepthFirstOrderFunction (object: any, parentKeyNameList?: (number | string)[], option?: FunctionOption): Array<(number | string)[]> {

  if (!object) { return [] }
  if (typeof object !== 'object') { return [] }

  if (!parentKeyNameList || parentKeyNameList.length < 0) {
    parentKeyNameList = []
  }

  let propertyKeyNameList: Array<(number | string)[]> = []

  if ((parentKeyNameList.length > 0) && (option ? !option.booleanObjectKeyNotAllowed : true)) {
    // Adding the parent key name list adds the object valued property keys to the result of this function.
    // To avoid adding any object valued property keys to the list, set booleanObjectKeyNotAllowed to true.
    propertyKeyNameList.push(parentKeyNameList)
  }

  for (let objectKey in object) {
    if (!object.hasOwnProperty(objectKey)) { continue }
    const objectPropertyKeyNameList = [...parentKeyNameList, objectKey]

    if (option && (option.objectPropertyTreeHeight !== undefined) && (objectPropertyKeyNameList.length > option.objectPropertyTreeHeight)) {
      return []
    }

    if (typeof object[objectKey] !== 'object') {
      propertyKeyNameList.push(objectPropertyKeyNameList)
      continue
    }

    propertyKeyNameList.push(...getObjectPropertyKeyNameListByDepthFirstOrderFunction(object[objectKey], objectPropertyKeyNameList, option))
  }

  return propertyKeyNameList
}

function getObjectPropertyKeyNameListByBreadthFirstOrderFunction (object: any, parentKeyNameList?: (number | string)[], option?: FunctionOption): Array<(number | string)[]> {

  if (!object) { return [] }
  if (typeof object !== 'object') { return [] }

  if (!parentKeyNameList || parentKeyNameList.length < 0) {
    parentKeyNameList = []
  }

  let propertyKeyNameList: Array<(number | string)[]> = Object.keys(object).map(objectKey => {
    const propertyKeyName = Array.isArray(object) ? parseInt(objectKey, 10) : objectKey
    return (parentKeyNameList as (number | string)[]).concat(propertyKeyName)
  })

  let removeObjectKeyNameListReferenceTable: { [propertyKeyNameListIndex: number]: boolean } = {}

  if (option && (option.objectPropertyTreeHeight !== undefined) && (propertyKeyNameList[0].length > option.objectPropertyTreeHeight)) {
    return []
  }

  for (let i = 0; i < propertyKeyNameList.length; i++) {
    const objectPropertyKeyNameList = propertyKeyNameList[i]
    const objectChildObjectValue = getObjectValue.function(object, objectPropertyKeyNameList)

    const objectChildPropertyKeyNameList = getObjectPropertyKeyNameListByBreadthFirstOrderFunction(objectChildObjectValue, objectPropertyKeyNameList, option)

    if (option && option.booleanObjectKeyNotAllowed && typeof objectChildObjectValue === 'object') {
      removeObjectKeyNameListReferenceTable[i] = true
    }

    propertyKeyNameList.push(...objectChildPropertyKeyNameList)
  }

  const propertyKeyNameListWithoutObjectKey = []

  // remove the object key list with the object key reference lists
  if (option && option.booleanObjectKeyNotAllowed) {
    for (let i = 0; i < propertyKeyNameList.length; i++) {
      const removeKeyNameListIndex = removeObjectKeyNameListReferenceTable[i]
      if (removeKeyNameListIndex) { continue }
      propertyKeyNameListWithoutObjectKey.push(propertyKeyNameList[i])
    }
    propertyKeyNameList = propertyKeyNameListWithoutObjectKey
  }

  return propertyKeyNameList
}

// Constants

const getObjectPropertyKeyNameList = {
  function: getObjectPropertyKeyNameListFunction
}

export {
  getObjectPropertyKeyNameList,
  FunctionOption,
  getObjectPropertyKeyNameListByDepthFirstOrderFunction,
  getObjectPropertyKeyNameListByBreadthFirstOrderFunction
}

