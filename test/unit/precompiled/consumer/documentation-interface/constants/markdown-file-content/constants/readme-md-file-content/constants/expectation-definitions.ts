


import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../../../@expectation-definitions/algorithms/get-project-file-path'
import { DOCUMENTATION_INTERFACE_MARKDOWN_FILE_CONTENT_README_MD_FILE_TEMPLATE } from './index'


describe(getProjectFilePath.function(__filename), () => {
  it('should contain a table in the file template', () => {
    expect(DOCUMENTATION_INTERFACE_MARKDOWN_FILE_CONTENT_README_MD_FILE_TEMPLATE).to.contain('|')
  })
})
