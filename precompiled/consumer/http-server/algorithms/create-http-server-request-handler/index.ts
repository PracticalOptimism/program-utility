


import * as express from 'express'

import { httpServerTable } from '../../variables/http-server'

function createHttpServerRequestHandlerFunction (httpServerId: string, httpMethodName: 'get' | 'put' | 'post' | 'delete' | 'patch', requestPathName: string, requestResponseFunction: express.RequestHandler): express.Express {
  const httpServer = httpServerTable[httpServerId]
  httpServer.httpServer![httpMethodName](requestPathName, requestResponseFunction)
  return httpServer.httpServer!
}

const createHttpServerRequestHandler = {
  function: createHttpServerRequestHandlerFunction
}

export {
  createHttpServerRequestHandler
}

