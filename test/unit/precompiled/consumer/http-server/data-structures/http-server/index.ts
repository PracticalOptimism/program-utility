
import * as express from 'express'
import * as http from 'http'
import * as httpStatus from 'http-status'

class HttpServer {
  httpServerId: string = ''
  serverPortTable: { [portNumber: number]: http.Server } = {}

  httpServer?: express.Express
}

class HttpServerRequestResponse {
  responseId: string = ''
  requestUrl: string = ''
  requestBody: any

  responseValue: any

  dateCreated: Date = new Date()

  statusCode: number = 200
  statusCodeDescription: string = httpStatus['200']

  hasErrorOccurred: boolean = false
  errorDescription: string = ''
}


export {
  HttpServer,
  HttpServerRequestResponse
}

