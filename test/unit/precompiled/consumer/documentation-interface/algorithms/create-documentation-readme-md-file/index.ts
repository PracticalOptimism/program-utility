
import * as fs from 'fs'

import { DOCUMENTATION_INTERFACE_MARKDOWN_FILE_CONTENT_README_MD_FILE_TEMPLATE } from '../../constants/markdown-file-content/constants/readme-md-file-content/constants'

// Algorithms

function createDocumentationReadmeMdFileFunction (filePath: string) {
  fs.writeFile(filePath, DOCUMENTATION_INTERFACE_MARKDOWN_FILE_CONTENT_README_MD_FILE_TEMPLATE, { flag: 'w', encoding: 'utf8' }, function () {
    console.log('Successfully created documentation README.md file at location: ', filePath)
  })
}

// Constants

const createDocumentationReadmeMdFile = {
  function: createDocumentationReadmeMdFileFunction
}

export {
  createDocumentationReadmeMdFile
}
