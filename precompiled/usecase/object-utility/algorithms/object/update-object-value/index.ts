
// Data Structures

class FunctionOption {
  deletePropertyKey?: boolean
  replaceStringObjectAtKeyIndex?: boolean
  constructor (defaultOption?: FunctionOption) {
    Object.assign(this, defaultOption)
  }
}

// Algorithms


function updateObjectValueFunction (object: any, keyNameList: (number | string)[], value: any, option?: FunctionOption): any {

  // create object or array if not already there
  if (!object) {
    if (typeof keyNameList[0] === 'number') {
      object = (new Array(keyNameList[0] + 1)).fill(undefined)
    } else {
      object = {}
    }
  }

  // return the original object if there are not any key names
  if (keyNameList.length === 0) {
    return object
  }

  // get the object value at child trees of the current object
  let objectValue = value
  if (keyNameList.length !== 1) {
    objectValue = updateObjectValueFunction(object[keyNameList[0]], keyNameList.slice(1), value, option)
  }

  // update the object value at this tree height in the recusive call
  if (typeof object === 'object') {
    object[keyNameList[0]] = objectValue
  } else if (typeof object === 'string') {
    const updateIndex = keyNameList[0] as number

    if (option && option.replaceStringObjectAtKeyIndex) {
      object = [object.slice(0, updateIndex), objectValue, object.slice(updateIndex + objectValue.length)].join('')
    } else {
      object = object.slice(0, updateIndex) + objectValue + object.slice(updateIndex)
    }
  }

  // remove the object key name from the key name list if specified by deletePropertyKey
  if ((option && option.deletePropertyKey) && (objectValue === undefined)) {
    delete object[keyNameList[0]]
  }

  return object
}

// Constants

const updateObjectValue = {
  function: updateObjectValueFunction
}

export {
  updateObjectValue,
  FunctionOption
}
