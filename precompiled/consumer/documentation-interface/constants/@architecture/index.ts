
import * as documentationInterface from '../documentation-interface'
import * as markdownFileContent from '../@architecture'

export {
  documentationInterface,
  markdownFileContent
}
