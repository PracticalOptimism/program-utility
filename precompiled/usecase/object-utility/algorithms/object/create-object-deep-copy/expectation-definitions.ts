
import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../@expectation-definitions/algorithms/get-project-file-path'
import { createObjectDeepCopy } from './index'
import { updateObjectValue } from '../update-object-value'
import { updateObjectValueByMapFunction } from '../update-object-value-by-map-function'
import { getObjectPropertyKeyNameList } from '../get-object-property-key-name-list'
import { getObjectValue } from '../get-object-value'
import { isObjectDeepEqual } from '../is-object-deep-equal'


describe(getProjectFilePath.function(__filename), () => {
  it('should deeply copy the object with an object property tree height number of 0', () => {
    const myObject = { englishName: 'Elizabeth', address: { city: 'Seoul, Korea' } }

    const myObjectCopy = createObjectDeepCopy.function(myObject, { deepObjectClonePropertyTreeHeight: 0 }) as typeof myObject

    // Change the property names of the original object
    myObject.englishName = 'Ally' // tree height number 1
    myObject.address.city = 'Beijing' // tree height number 2

    // Expect the english names to equal since the copy tree height is shallow enough to not cover 'englishName'
    expect(myObject.englishName).to.equal(myObjectCopy.englishName)

    // Expect the city names to equal since the copy tree height is shallow enough to not cover ['address', 'city']
    expect(myObject.address.city).to.equal(myObjectCopy.address.city)
  })
  it('should deeply copy the object with an object property tree height number of 1', () => {
    const myObject = { englishName: 'Elizabeth', address: { city: 'Seoul, Korea' } }

    const myObjectCopy = createObjectDeepCopy.function(myObject, { deepObjectClonePropertyTreeHeight: 1 }) as typeof myObject

    // Change the property names of the original object
    myObject.englishName = 'Ally' // tree height number 1
    myObject.address.city = 'Beijing' // tree height number 2

    // Expect the english names to equal since the copy tree height is shallow enough to not cover 'englishName'
    expect(myObject.englishName).to.not.equal(myObjectCopy.englishName)

    // Expect the city names to not equal since the copy tree height is shallow enough to not cover ['address', 'city']
    expect(myObject.address.city).to.equal(myObjectCopy.address.city)
  })
  it('should deeply copy the object with an object property tree height number of Infinity', () => {
    const myObject = { englishName: 'Elizabeth', address: { city: 'Seoul, Korea' } }

    const myObjectCopy = createObjectDeepCopy.function(myObject, { deepObjectClonePropertyTreeHeight: Infinity }) as typeof myObject

    // Change the property names of the original object
    myObject.englishName = 'Ally' // tree height number 1
    myObject.address.city = 'Beijing' // tree height number 2

    // Expect the english names to not equal since the copy tree height is deep enough to cover 'englishName'
    expect(myObject.englishName).to.not.equal(myObjectCopy.englishName)

    // Expect the city names to not equal since the copy tree height is deep enough to cover ['address', 'city']
    expect(myObject.address.city).to.not.equal(myObjectCopy.address.city)
  })
  it('should deeply copy the object without an object property tree height number', () => {
    const myObject = { englishName: 'Elizabeth', address: { city: 'Seoul, Korea' } }

    const myObjectCopy = createObjectDeepCopy.function(myObject) as typeof myObject

    // Change the property names of the original object
    myObject.englishName = 'Ally' // tree height number 1
    myObject.address.city = 'Beijing' // tree height number 2

    // Expect the english names to not equal since the copy tree height is deep enough to cover 'englishName'
    expect(myObject.englishName).to.not.equal(myObjectCopy.englishName)

    // Expect the city names to not equal since the copy tree height is deep enough to cover ['address', 'city']
    expect(myObject.address.city).to.not.equal(myObjectCopy.address.city)
  })
  it('should deeply copy the object at an arbitrary random number property tree height', () => {
    let myObject = {}

    for (let i = 0; i < 100; i++) {
      const propertyKeyNameList = [...(new Array(Math.floor(Math.random() * 10)))].map(() => Math.random().toString())
      updateObjectValue.function(myObject, propertyKeyNameList, { value: Math.random().toString() })
    }

    const randomObjectTreeHeight = Math.floor(Math.random() * 10)
    let myObjectChangedValueTable: { [keyNameListConcatenatedString: string]: boolean } = {}

    // Create a deep copy of the object for properties above the random object tree height number
    const myObjectCopy = createObjectDeepCopy.function(myObject, { deepObjectClonePropertyTreeHeight: randomObjectTreeHeight }) as typeof myObject

    // Change all the values of the object property key name lists and track if we actually do so
    updateObjectValueByMapFunction.function(myObject, (objectPropertyKeyNameList: (number | string)[], objectPropertyValue: any) => {
      const originalPropertyValue = createObjectDeepCopy.function(objectPropertyValue)
      let newPropertyValue: any
      if (typeof objectPropertyValue === 'object') {
        if (objectPropertyValue.value) {
          objectPropertyValue.value = Math.random().toString()
          newPropertyValue = objectPropertyValue
        } else {
          newPropertyValue = objectPropertyValue
        }
      } else {
        newPropertyValue = Math.random().toString()
      }

      if (!isObjectDeepEqual.function(originalPropertyValue, newPropertyValue)) {
        myObjectChangedValueTable[objectPropertyKeyNameList.join('')] = true
      }

      return newPropertyValue
    })

    const myObjectPropertyKeyNameList = getObjectPropertyKeyNameList.function(myObject)

    // console.log('random tree height: ', randomObjectTreeHeight)
    // console.log('greatest tree height: ', Math.max(...myObjectPropertyKeyNameList.map(list => list.length)))

    for (let i = 0; i < myObjectPropertyKeyNameList.length; i++) {
      const propertyKeyNameList = myObjectPropertyKeyNameList[i]
      const currentObjectPropertyTreeHeight = propertyKeyNameList.length

      const myObjectValue = getObjectValue.function(myObject, propertyKeyNameList)
      const myObjectCopyValue = getObjectValue.function(myObjectCopy, propertyKeyNameList)

      if (!myObjectChangedValueTable[propertyKeyNameList.join('')]) { continue }

      if ((currentObjectPropertyTreeHeight > randomObjectTreeHeight) ||
          (typeof myObjectValue === 'object') &&
          (currentObjectPropertyTreeHeight + 1 > randomObjectTreeHeight)) {
        // Expect there to be no changes to the objects since the objects should share memory
        // beyond the random object tree height number
        if (typeof myObjectValue === 'object') {
          expect(myObjectValue).to.deep.equal(myObjectCopyValue)
        }
      } else {
        // Expect there to be differences between the object since objects are deeply copied
        if (typeof myObjectValue === 'object') {
          expect(myObjectValue).to.not.deep.equal(myObjectCopyValue)
        }
      }
    }
  })
  it('should not change the array type to an object when deep copying', () => {
    const myObject = { englishName: 'Richard Cooling', address: { city: 'Nigeria, Africa' }, baseballCardCollectionList: [{ cardNumber: '123' }, { cardNumber: '456' }] }

    const myObjectCopy = createObjectDeepCopy.function(myObject) as typeof myObject

    expect(myObjectCopy.baseballCardCollectionList).to.deep.equal(myObject.baseballCardCollectionList)
  })
})

