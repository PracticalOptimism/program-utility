

import { getObjectPropertyValueList } from '../../../../../../../../usecase/object-utility/algorithms/object/get-object-property-value-list'

// Create Application Programmable Interface Table

function createApplicationProgrammableInterfaceTableFunction (documentationObject: any): string {
  const propertyStringIdList: string[] = ['']
  let textString = ''

  const valueList = getObjectPropertyValueList.function(documentationObject, {
    returnObjectPropertyKeyListPair: true,
    propertyKeyNameListOrderType: 'depthFirstOrder'
  })

  textString += `\n<pre><details open><summary>📁 API Documentation Reference (Click to Open)</summary>\n`

  // Update the text string to contain the table
  for (let valueListIndex = 0; valueListIndex < valueList.length; valueListIndex++) {
    const objectPropertyKeyList = valueList[valueListIndex].objectPropertyKeyList
    const objectPropertyValue = valueList[valueListIndex].objectPropertyValue
    const propertyStringId = objectPropertyKeyList.join('')

    // close the previous open <details> using </details>
    for (let propertyStringIdListIndex = propertyStringIdList.length - 1; propertyStringIdListIndex > 0; propertyStringIdListIndex--) {
      const earlierPropertyStringId = propertyStringIdList[propertyStringIdListIndex]

      if (earlierPropertyStringId === '' || earlierPropertyStringId === propertyStringId) { continue }

      const isAncestorToProperty = propertyStringId.indexOf(earlierPropertyStringId) === 0

      if (isAncestorToProperty) { break }

      propertyStringIdList.pop()
      textString += `\n</details></pre>`
    }

    // Add the <details> opening tag
    if (typeof objectPropertyValue === 'object') {
      propertyStringIdList.push(propertyStringId)
      textString += `\n\n<pre><details><summary>📁 ${createContentTitleFromObjectPropertyKeyList(objectPropertyKeyList)}</summary>`
    }

    // Add the content of this object property value
    textString += createApplicationProgrammableInterfaceContentForObject(objectPropertyKeyList, objectPropertyValue)

    // if this is the last element, ensure all the </details> are closed . . . otherwise, continue
    if (valueListIndex !== valueList.length - 1) {
      continue
    }

    for (let propertyStringIdListIndex = propertyStringIdList.length - 1; propertyStringIdListIndex > 0; propertyStringIdListIndex--) {
      propertyStringIdList.pop()
      textString += `\n</details></pre>`
    }
  }

  textString += `\n</details></pre>`

  return textString
}

function createApplicationProgrammableInterfaceContentForObject (objectPropertyKeyList: (number | string)[], objectValue: any): string {
  let textString = ''

  textString += `<h5>${createContentTitleFromObjectPropertyKeyList(objectPropertyKeyList)}</h5>`

  // Return early for non-object data structures
  if (typeof objectValue !== 'object') {
    textString += objectValue
    return textString
  }

  // For object data structures, do something special :)

  textString += [].join('\n\n')

  return textString
}

function createContentTitleFromObjectPropertyKeyList (objectPropertyKeyList: (number | string)[]): string {
  let contentTitle: string = `${objectPropertyKeyList[objectPropertyKeyList.length - 1]}`
  // Remove 'Documentation' text
  const documentationTextIndex = contentTitle.indexOf('Documentation')
  if (documentationTextIndex >= 0) {
    contentTitle = contentTitle.slice(0, documentationTextIndex)
  }
  // Capitalize the first alphabetical character
  let firstAlphabeticalCharacterIndex = 0
  for (let characterIndex = 0; characterIndex < contentTitle.length; characterIndex++) {
    const isAlphabeticalCharacter = contentTitle.charAt(characterIndex).match(/^[A-Za-z]+$/) ? true : false
    if (!isAlphabeticalCharacter) { continue }
    firstAlphabeticalCharacterIndex = characterIndex
    break
  }
  contentTitle = [contentTitle.slice(0, firstAlphabeticalCharacterIndex), contentTitle.charAt(firstAlphabeticalCharacterIndex).toUpperCase(), contentTitle.slice(firstAlphabeticalCharacterIndex + 1)].join('')
  // Replace '_' with '@' symbol
  contentTitle = contentTitle.charAt(0) === '_' ? contentTitle.replace('_', '@') : contentTitle
  return contentTitle
}

const createApplicationProgrammableInterfaceTable = {
  function: createApplicationProgrammableInterfaceTableFunction
}

export {
  createApplicationProgrammableInterfaceTable
}

