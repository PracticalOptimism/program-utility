
import { ObjectDuplicator } from '../../../data-structures/object-duplicator'

// Algorithms

function deleteDuplicateFunction (objectDuplicator: ObjectDuplicator<any>, duplicateId: string) {
  delete objectDuplicator.objectDuplicateTable[duplicateId]
}

// Constants

const deleteDuplicate = {
  function: deleteDuplicateFunction
}

export {
  deleteDuplicate
}

