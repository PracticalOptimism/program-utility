
import * as algorithms from '../algorithms/@architecture'
import * as dataStructures from '../data-structures/@architecture'

export {
  algorithms,
  dataStructures
}

