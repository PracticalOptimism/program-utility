

// Algorithms

function isObjectDeepEqualFunction (object1: any, object2: any): boolean {

  if (typeof object1 !== 'object') { return false }
  if (typeof object1 !== typeof object2) { return false }
  if (((object1 instanceof Array) && !(object2 instanceof Array)) || (!(object1 instanceof Array) && (object2 instanceof Array))) { return false }

  for (let propertyKeyName in object1) {
    if (!object1.hasOwnProperty(propertyKeyName)) { continue }
    if (!object2.hasOwnProperty(propertyKeyName)) { return false }
    if (typeof object1[propertyKeyName] === 'object') { continue }
    if (object1[propertyKeyName] !== object2[propertyKeyName]) { return false }
  }

  for (let propertyKeyName in object2) {
    if (!object2.hasOwnProperty(propertyKeyName)) { continue }
    if (!object1.hasOwnProperty(propertyKeyName)) { return false }
    if (typeof object2[propertyKeyName] === 'object') { continue }
    if (object2[propertyKeyName] !== object2[propertyKeyName]) { return false }
  }

  return true
}

// Constants

const isObjectDeepEqual = {
  function: isObjectDeepEqualFunction
}

export {
  isObjectDeepEqual
}

