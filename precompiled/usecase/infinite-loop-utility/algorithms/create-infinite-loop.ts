
import { InfiniteLoop } from '../data-structures/infinite-loop'
import { infiniteLoopTable } from '../variables/infinite-loop'
import { startInfiniteLoop } from './start-infinite-loop'

function createInfiniteLoopFunction (infiniteLoopAlgorithmFunction: Function, startDate: Date, millisecondRegularDelayTime: number): InfiniteLoop {

  const infiniteLoop = new InfiniteLoop()
  infiniteLoop.loopId = infiniteLoopAlgorithmFunction.toString()
  infiniteLoop.startDate = startDate
  infiniteLoop.millisecondRegularDelayTime = millisecondRegularDelayTime
  infiniteLoop.algorithm = infiniteLoopAlgorithmFunction
  infiniteLoopTable[infiniteLoop.loopId] = infiniteLoop

  infiniteLoop.isStarted = true

  const delayTimeToStart = Math.abs(((new Date()).getMilliseconds() - (new Date(startDate)).getMilliseconds()) % millisecondRegularDelayTime)

  startInfiniteLoop.function(infiniteLoop, delayTimeToStart + millisecondRegularDelayTime)

  return infiniteLoop
}

const createInfiniteLoop = {
  function: createInfiniteLoopFunction
}

export {
  createInfiniteLoop
}
