
import { InfiniteLoop } from '../data-structures/infinite-loop'

const infiniteLoopTable: { [infiniteLoopId: string]: InfiniteLoop } = {}

export {
  infiniteLoopTable
}
