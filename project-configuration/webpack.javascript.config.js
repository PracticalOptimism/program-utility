
const webpackCommonConfig = require('./webpack.common.config')

module.exports = {
  ...webpackCommonConfig,
  target: 'web',
  mode: 'development',
  entry: {
    'data-structures/javascript-library/javascript': './precompiled/javascript.ts'
  }
}



