

import * as _projectDocumentation from '../../../../@project/@architecture/documentation'
import * as usecaseDocumentation from '../../../../usecase/@architecture/documentation'


// Constants

const DOCUMENTATION_INTERFACE_LIBRARY_INSTANCE_MODEL = {
  _projectDocumentation,
  consumerDocumentation: {
    documentationInterfaceDocumentation: {}
    // javascriptDocumentation: { }
    // httpServerDocumentation: { }
  },
  usecaseDocumentation,
  providerDocumentation: {}
}

export {
  DOCUMENTATION_INTERFACE_LIBRARY_INSTANCE_MODEL
}

/*

projectDirectory
projectSoftwareDirectory
projectSoftwareProgramDirectory

documentationDemo
documentationDemoInstallation
documentationDemoInstallationProgram
documentationDemoInstallationPage
documentationDemoInstallationProgramPage
documentationDemoInstallationPageProgram
documentationDemoPageProgram
documentationDemoPageProgramPage
documentationDemoPageProgramPageDemo
pageDemoDocumentationPageFormat
pageDemoDocumentationPage
pageDemoDocumentation
pageDemoDocumentation

documentationInterfaceDemoPage
documentationInterfaceDemoPageStart
documentationInterfaceDemoPage
documentationIterfacePageDemo
documentationInterfacePageDemoPageStart


Problem #1: no agreed upon format for documentation demo page
- Ideas: tables, drop down lists, side page menu
- Ideas: buttons, text fields, text area

Examples of Good Documentation Pages
- Vue.js website
-


Idea #1: generate a documentation demo page from a javascript object
Idea #2: use vue.js with vuetify to generate an html/css/javascript page
Idea #3: use stackblitz to generate the page window where the documentation will be
Idea #4: documentation can be in any project, but the documentation demo generator code
        is reusable by other projects for example programUtility.usecase.documentationUtility





*/

