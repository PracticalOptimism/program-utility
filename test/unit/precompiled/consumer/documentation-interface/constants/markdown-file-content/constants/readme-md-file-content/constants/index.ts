
import { DOCUMENTATION_INTERFACE_LIBRARY_INSTANCE_MODEL } from '../../../../documentation-interface'
import * as projectConstants from '../../../../../../../@project/constants/project'

import { createApplicationProgrammableInterfaceTable } from '../algorithms/create-application-programmable-interface-table'
import { createProjectOperatingEnvironmentTable } from '../algorithms/create-project-operating-environment-table'
import { createRelatedWorkTable } from '../algorithms/create-related-work-table'

// Constants

const DOCUMENTATION_INTERFACE_MARKDOWN_FILE_CONTENT_README_MD_FILE_TEMPLATE = `
# ${projectConstants.PROJECT_NAME}

### ${projectConstants.PROJECT_TYPE_TEXT_DESCRIPTION}

${
`##### Website Resources\n` +
`[Source Code](${projectConstants.PROJECT_SOURCE_CODE_URL})
&nbsp;| [Demos](${projectConstants.PROJECT_DEMOS_OPTION_URL})
&nbsp;| [Video Introduction](${projectConstants.PROJECT_VIDEO_INTRODUCTION_URL})
&nbsp;| [Video Tutorial](${projectConstants.PROJECT_VIDEO_TUTORIAL})
&nbsp;| [Live Programming Development Journal](${projectConstants.PROJECT_VIDEO_DEVELOPMENT_JOURNAL_URL})
`.replace(/(\r\n|\n|\r)/gm, '') // remove line break for single line formatting
}

${
`##### Project Development Status Updates\n` +
`[![npm version](${projectConstants.PROJECT_DEVELOPMENT_STATUS_UPDATE_BADGE_OBJECT.npmVersionBadgeUrl})](${projectConstants.PROJECT_DEVELOPMENT_STATUS_UPDATE_BADGE_OBJECT.npmVersionResourceUrl})
&nbsp;[![downloads](${projectConstants.PROJECT_DEVELOPMENT_STATUS_UPDATE_BADGE_OBJECT.downloadsBadgeUrl})](${projectConstants.PROJECT_DEVELOPMENT_STATUS_UPDATE_BADGE_OBJECT.npmVersionResourceUrl})
&nbsp;[![coverage report](${projectConstants.PROJECT_DEVELOPMENT_STATUS_UPDATE_BADGE_OBJECT.coverageReportBadgeUrl})](${projectConstants.PROJECT_DEVELOPMENT_STATUS_UPDATE_BADGE_OBJECT.coverageReportResourceUrl})
&nbsp;[![pipeline status](${projectConstants.PROJECT_DEVELOPMENT_STATUS_UPDATE_BADGE_OBJECT.pipelineStatusBadgeUrl})](${projectConstants.PROJECT_DEVELOPMENT_STATUS_UPDATE_BADGE_OBJECT.pipelineStatusResourceUrl})
`.replace(/(\r\n|\n|\r)/gm, '') // remove line break for single line formatting
}

##### This Documentation Page was last Updated on ${new Date()}

### Installation

##### Node Package Manager (NPM) Installation
\`\`\`
npm install --save ${projectConstants.PROJECT_JAVASCRIPT_OBJECT_PACKAGE_DESCRIPTION.npmPackageId}
\`\`\`

##### Script import from JavaScript (requires NPM installation)
\`\`\`javascript
import * as ${projectConstants.PROJECT_JAVASCRIPT_LIBRARY_ID} from '${projectConstants.PROJECT_JAVASCRIPT_OBJECT_PACKAGE_DESCRIPTION.npmPackageId}'
\`\`\`

##### HTML Script Import
\`\`\`html
<script src="${projectConstants.PROJECT_JAVASCRIPT_OBJECT_PACKAGE_DESCRIPTION.htmlScriptSrcId}"></script>
\`\`\`

### Getting Started: Accessing Object Property Value

\`\`\`javascript
// Initialize the javascript library instance and the usecase property value
const ${projectConstants.PROJECT_JAVASCRIPT_LIBRARY_ID}Instance = ${projectConstants.PROJECT_JAVASCRIPT_LIBRARY_ID}.algorithms.createJavascriptLibraryInstance.function()
const objectUtility =  ${projectConstants.PROJECT_JAVASCRIPT_LIBRARY_ID}Instance.usecase.objectUtility.algorithms.object

// Create your javascript object
const object = { name: 'Hitomi', hobbyTable: { dancing: true, watchingScaryMovies: false } }

// Create your property value key list
const propertyKeyList = ['hobbyTable', 'dancing']

// Get your object property value using a key list
const propertyValue = objectUtility.getObjectValue.function(object, propertyKeyList)

// The value printed should return 'true'
console.log(propertyValue)
\`\`\`

### Getting Started: Creating Object Duplicators

\`\`\`javascript
// Initialize the javascript library instance and the usecase property value
const ${projectConstants.PROJECT_JAVASCRIPT_LIBRARY_ID}Instance = ${projectConstants.PROJECT_JAVASCRIPT_LIBRARY_ID}.algorithms.createJavascriptLibraryInstance.function()
const  objectDuplicatorUtility =  ${projectConstants.PROJECT_JAVASCRIPT_LIBRARY_ID}Instance.usecase.objectUtility.algorithms.objectDuplicator

// Create your javascript object
const object = { name: 'Miranda', hobbyTable: { watchingScaryMovies: true, watchingDanceRecitals: false } }

// Create your ObjectDuplicator data structure
const objectDuplicator = objectDuplicatorUtility.createObjectDuplicator.function(object)

// Add a duplicate of your object
objectDuplicatorUtility.addObjectToObjectDuplicator.function(objectDuplicator, 'item1')

// Get the duplicate of your object by using a sting value (objectDuplicateId)
const objectDuplicate = objectDuplicatorUtility.getObjectFromObjectDuplicator.function('item1')

// Change the 'name' property value of the object duplicate
// to showcase that the object and object duplicate
// will have different names since they are deep copies
// of one another.
objectDuplicate.name = 'Susan'

// The value printed should return 'false'
console.log(object.name === objectDuplicate.name)

\`\`\`

### Application Programmable Interface (API Reference)

${ // Application Programmable Interface (API Reference) Details Nested Table
  createApplicationProgrammableInterfaceTable.function(DOCUMENTATION_INTERFACE_LIBRARY_INSTANCE_MODEL)
}

${ // Project Operating Environment Table
  createProjectOperatingEnvironmentTable.function()
}

### To Resolve Problems and Submit Feature Requests

To report issues or submit feature requests with this projectConstants, please visit [${projectConstants.PROJECT_NAME} Issue Tracker](${projectConstants.PROJECT_PROBLEM_RESOLUTION_URL})

### About This Project
${projectConstants.PROJECT_TEXT_DESCRIPTION}

### Benefits
${projectConstants.PROJECT_BENEFIT_LIST.map((benefitTextDescription: string) => `- ${benefitTextDescription}`).join('\n')}

### Features
${projectConstants.PROJECT_FEATURE_LIST.map((featureTextDescription: string) => `- ${featureTextDescription}`).join('\n')}

### Limitations
${projectConstants.PROJECT_LIMITATION_LIST.map((limitationTextDescription: string) => `- ${limitationTextDescription}`).join('\n')}

### Related Work
${projectConstants.PROJECT_RELATED_WORK_TEXT_DESCRIPTION}

${ // Related Work Table
  createRelatedWorkTable.function()
}

### Acknowledgements

### License
MIT

`

export {
  DOCUMENTATION_INTERFACE_MARKDOWN_FILE_CONTENT_README_MD_FILE_TEMPLATE
}

