
import { httpServerTable } from '../../variables/http-server'

function stopListeningToHttpServerRequestsFunction (httpServerId: string, portNumber: number) {
  const httpServer = httpServerTable[httpServerId]

  if (!httpServer) {
    throw Error(`Cannot find http server by httpServerId: ${httpServerId}`)
  }

  const httpServerPort = httpServer.serverPortTable[portNumber]

  if (!httpServerPort) {
    throw Error(`Cannot find http server port by portNumber: ${portNumber}`)
  }

  httpServerPort.close()
}

const stopListeningToHttpServerRequests = {
  function: stopListeningToHttpServerRequestsFunction
}

export {
  stopListeningToHttpServerRequests
}
