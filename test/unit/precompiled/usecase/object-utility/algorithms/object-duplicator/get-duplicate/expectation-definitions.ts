
import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../@expectation-definitions/algorithms/get-project-file-path'

describe(getProjectFilePath.function(__filename), () => {
  it(`should do something for ${__filename.split('/')[__filename.split('/').length - 1]}`, () => {
    expect(2).to.equal(2)
  })
})

