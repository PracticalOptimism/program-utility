
import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../@expectation-definitions/algorithms/get-project-file-path'
import { updateObjectValue } from './index'

describe(getProjectFilePath.function(__filename), () => {
  it('should update the value of an object', () => {
    const myObject = { englishName: 'Elizabeth', address: { city: 'Seoul, Korea' } }

    updateObjectValue.function(myObject, ['galacticAddress', 'starCluster', 'starSystem', 'starId'], 'centrum-lux')

    expect((myObject as any).galacticAddress.starCluster.starSystem.starId).to.equal('centrum-lux')
  })
  it('should remove the object property key if deletePropertyKey', () => {
    const myObject = { englishName: 'Elizabeth', address: { city: 'Seoul, Korea' } }

    updateObjectValue.function(myObject, ['galacticAddress', 'starCluster', 'starSystem', 'starId'], undefined, { deletePropertyKey: true })

    expect((myObject as any).galacticAddress.starCluster.starSystem).to.deep.equal({})
  })
  it('should create an item that is an array if the type of property is a number', () => {
    const myObject = { englishName: 'Elizabeth', address: { city: 'Seoul, Korea' } }

    updateObjectValue.function(myObject, ['favoriteFruitList', 0], 'Banana')
    updateObjectValue.function(myObject, ['favoriteFruitList', 1], 'Kiwi')
    updateObjectValue.function(myObject, ['favoriteFruitList', 2], 'Mango')

    expect((myObject as any).favoriteFruitList).to.deep.equal(['Banana', 'Kiwi', 'Mango'])
  })
  it('should update the value of a string with or without replacement of text characters', () => {
    const valueWithReplacement = updateObjectValue.function('no-no-no', [0], 'ye-', { replaceStringObjectAtKeyIndex: true })
    const valueWithoutReplacement = updateObjectValue.function('no-no-no', [0], 'ye-')

    expect(valueWithReplacement).to.equal('ye-no-no')
    expect(valueWithoutReplacement).to.equal('ye-no-no-no')
  })
})

