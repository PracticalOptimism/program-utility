

import { TableFormattedByHorizontalList, TableFormattedByVerticalList } from '../../../data-structures/documentation-interface'

// Algorithms

function createMarkdownTableFromJavascriptObjectFunction (documentationInterfaceTable: TableFormattedByHorizontalList | TableFormattedByVerticalList): string {
  let tableRowList: string[][] = []

  const tableType: string = (documentationInterfaceTable as any).tableColumnListTable ? TableFormattedByVerticalList.name : TableFormattedByHorizontalList.name

  // Condition #1: vertical list formatted table
  if (tableType === TableFormattedByVerticalList.name) {
    tableRowList = createMarkdownTableFromJavascriptObjectByVerticalListFormattedTableFunction(documentationInterfaceTable as TableFormattedByVerticalList)
  // Condition #2: horizontal list formatted table
  } else if (tableType === TableFormattedByHorizontalList.name) {
    tableRowList = createMarkdownTableFromJavascriptObjectByHorizontalListFormattedTableFunction(documentationInterfaceTable as TableFormattedByHorizontalList)
  }

  return tableRowList.map((tableRowListItem) => `| ${tableRowListItem.join(' | ')} |`).join('\n')
}

function createMarkdownTableFromJavascriptObjectByVerticalListFormattedTableFunction (documentationInterfaceTable: TableFormattedByVerticalList): string[][] {
  let tableRowList: string[][] = []
  let verticalListFormattedTable = documentationInterfaceTable

  let tableColumnList = verticalListFormattedTable.tableColumnPropertyList.map((columnPropertyItem) => {
    const columnPropertyList = verticalListFormattedTable.tableColumnListTable[columnPropertyItem.columnListTableId]
    columnPropertyList.unshift(columnPropertyItem.columnPropertyId)
    return columnPropertyList
  })

  // add the table row dash list ('--') after the first row
  tableColumnList = tableColumnList.map((tableColumn) => {
    return [tableColumn[0], '--', ...tableColumn.slice(1)]
  })

  const tallestColumnListLength = Math.max(...tableColumnList.map((tableColumn) => tableColumn.length))

  // initialize the table row list
  for (let rowIndex = 0; rowIndex < tallestColumnListLength; rowIndex++) {
    let tableColumnRowList = []
    for (let tableColumnListItemIndex = 0; tableColumnListItemIndex < tableColumnList.length; tableColumnListItemIndex++) {
      const tableColumnListItem = tableColumnList[tableColumnListItemIndex]
      if (tableColumnListItem[rowIndex]) {
        tableColumnRowList.push(tableColumnListItem[rowIndex])
      } else {
        tableColumnRowList.push('--')
      }
    }
    tableRowList.push(tableColumnRowList)
  }

  return tableRowList
}

function createMarkdownTableFromJavascriptObjectByHorizontalListFormattedTableFunction (documentationInterfaceTable: TableFormattedByHorizontalList) {
  let tableRowList: string[][] = []

  // create a list of the table rows (table rows are arrays of values)
  tableRowList = documentationInterfaceTable.tableRowPropertyList.map((rowPropertyItem) => {
    const rowPropertyList = documentationInterfaceTable.tableColumnPropertyList.map((columnPropertyItem: any) => {
      return columnPropertyItem[rowPropertyItem.columnPropertyId]
    })
    rowPropertyList.unshift(rowPropertyItem.rowPropertyId)
    return rowPropertyList
  })

  if (tableRowList.length === 0 || tableRowList[0].length === 0) {
    return []
  }

  // create a list of dashes ('--') with the same number of elements as the first table row
  const tableHeaderRowDashList = (new Array(tableRowList[0].length)).fill('--')

  // add the table row dash list after the first row
  tableRowList = [tableRowList[0], tableHeaderRowDashList, ...tableRowList.slice(1)]
  return tableRowList
}

// Constants

const createMarkdownTableFromJavascriptObject = {
  function: createMarkdownTableFromJavascriptObjectFunction
}

export {
  createMarkdownTableFromJavascriptObject,
  createMarkdownTableFromJavascriptObjectByVerticalListFormattedTableFunction,
  createMarkdownTableFromJavascriptObjectByHorizontalListFormattedTableFunction
}

