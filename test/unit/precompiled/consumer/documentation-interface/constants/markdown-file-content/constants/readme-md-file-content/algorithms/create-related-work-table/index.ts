
import { PROJECT_RELATED_WORK_TABLE } from '../../../../../../../../@project/constants/project'
import { createMarkdownTableFromJavascriptObject } from '../../../../../../../../usecase/documentation-utility/algorithms/markdown/create-markdown-table-from-javascript-object'
import { updateObjectValueByMapFunction } from '../../../../../../../../usecase/object-utility/algorithms/object/update-object-value-by-map-function'


// Create Related Work Table
function createRelatedWorkTableFunction (): string {
  return createMarkdownTableFromJavascriptObject.function({
    ...PROJECT_RELATED_WORK_TABLE.projectRelatedWorkTableObject,
    tableColumnListTable: {
      ...updateObjectValueByMapFunction.function(PROJECT_RELATED_WORK_TABLE.projectRelatedWorkTableObject.tableColumnListTable, (tableColumnListTableKeyList: (number | string)[], tableColumnListTableValue: any) => {
        if (tableColumnListTableKeyList.join('') !== 'objectUtilityProviderList') {
          return tableColumnListTableValue
        }
        tableColumnListTableValue = (tableColumnListTableValue as any[]).map((tableColumnListTableValueItem, itemIndex) => {
          tableColumnListTableValueItem.providerName = `[${itemIndex + 1}] **${tableColumnListTableValue.providerName}**: `
          tableColumnListTableValueItem.providerResourceUrl = `[${tableColumnListTableValue.providerResourceUrl}](${tableColumnListTableValue.providerResourceUrl})`
        })
        return tableColumnListTableValue
      }, { objectPropertyTreeHeight: 1, shouldCreateDeepObjectClone: true })
    }
  })
}

const createRelatedWorkTable = {
  function: createRelatedWorkTableFunction
}

export {
  createRelatedWorkTable
}

