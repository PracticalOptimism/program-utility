
import * as createDuplicateDocumentation from '../create-duplicate/documentation'
import * as createObjectDuplicatorDocumentation from '../create-object-duplicator/documentation'
import * as getDuplicateDocumentation from '../get-duplicate/documentation'
import * as removeDuplicateDocumentation from '../delete-duplicate/documentation'


export {
  createDuplicateDocumentation,
  createObjectDuplicatorDocumentation,
  getDuplicateDocumentation,
  removeDuplicateDocumentation
}

