
import * as algorithmsDocumentation from '../algorithms/@architecture/documentation'
import * as dataStructuresDocumentation from '../data-structures/@architecture/documentation'

export {
  algorithmsDocumentation,
  dataStructuresDocumentation
}

