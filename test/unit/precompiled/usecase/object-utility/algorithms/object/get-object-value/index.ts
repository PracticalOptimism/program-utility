

// Algorithms

function getObjectValueFunction (object: any, propertyKeyNameList: (number | string)[]): any {
  if (!object) {
    return undefined
  }
  if (propertyKeyNameList.length === 0) {
    return object
  }
  if (!object[propertyKeyNameList[0]]) {
    return undefined
  }
  if (propertyKeyNameList.length === 1) {
    return object[propertyKeyNameList[0]]
  }
  return getObjectValueFunction(object[propertyKeyNameList[0]], propertyKeyNameList.slice(1))
}

// Constants

const getObjectValue = {
  function: getObjectValueFunction
}

export {
  getObjectValue
}

