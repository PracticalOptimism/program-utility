
class InfiniteLoop {
  loopId: string = ''

  startDate: Date = new Date()
  millisecondRegularDelayTime: number = 0

  isStarted: boolean = false
  setTimeoutId: any = null

  algorithm: Function = () => {/* */}

  constructor (millisecondRegularDelayTime?: number) {
    if (millisecondRegularDelayTime && millisecondRegularDelayTime < 0) {
      millisecondRegularDelayTime = 0
    }
    this.millisecondRegularDelayTime = millisecondRegularDelayTime || 0
  }
}

export {
  InfiniteLoop
}
