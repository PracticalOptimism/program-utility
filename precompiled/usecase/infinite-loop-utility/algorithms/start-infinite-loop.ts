

import { InfiniteLoop } from '../data-structures/infinite-loop'

function startInfiniteLoopFunction (infiniteLoop: InfiniteLoop, delayTimeToStart: number) {
  infiniteLoop.algorithm()

  if (!infiniteLoop.isStarted) { return }

  infiniteLoop.setTimeoutId = setTimeout(async () => {
    startInfiniteLoop.function(infiniteLoop, infiniteLoop.millisecondRegularDelayTime)
  }, delayTimeToStart)
}

const startInfiniteLoop = {
  function: startInfiniteLoopFunction
}

export {
  startInfiniteLoop
}
