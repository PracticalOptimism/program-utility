
import * as objectUtilityDocumentation from '../object-utility/@architecture/documentation'
import * as documentationUtilityDocumentation from '../documentation-utility/@architecture/documentation'

export {
  objectUtilityDocumentation,
  documentationUtilityDocumentation
}

