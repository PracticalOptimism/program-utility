
import { createObjectDeepCopy } from '../create-object-deep-copy'
import { getObjectValue } from '../get-object-value'
import { getObjectPropertyKeyNameList } from '../get-object-property-key-name-list'
import { deleteObjectValue } from '../delete-object-value'
import { updateObjectValue } from '../update-object-value'

// Data Structures

class FunctionOption {
  objectPropertyTreeHeight?: number
  shouldCreateDeepObjectClone?: boolean
  deepObjectClonePropertyTreeHeight?: number
  booleanObjectKeyNotAllowed?: boolean
  propertyKeyNameListOrderType?: 'breadthFirstOrder' | 'depthFirstOrder'

  constructor (defaultOption?: FunctionOption) {
    Object.assign(this, defaultOption)
  }
}

declare type ObjectMap = { objectPropertyKeyList: (number | string)[], objectPropertyValue: any }
const ObjectMapFunction: (objectPropertyKeyList: (number | string)[], objectPropertyValue: any, objectPropertyListIndex?: number) => { objectPropertyKeyList?: (number | string)[], objectPropertyValue?: any } | any = () => {/* */}

// Algorithms

function updateObjectValueByMapFunctionFunction (object: any, mapFunction: typeof ObjectMapFunction, option?: FunctionOption): any {

  if (option && option.shouldCreateDeepObjectClone) {
    object = createObjectDeepCopy.function(object, option)
  }

  const objectMapResultList: Array<ObjectMap> = getObjectPropertyKeyNameList.function(object, [], option).map((originalObjectPropertyKeyList: (number | string)[], listIndex: number) => {
    let objectPropertyKeyList: (number | string)[] = originalObjectPropertyKeyList
    let objectPropertyValue: any
    const objectMapResult = mapFunction(objectPropertyKeyList, getObjectValue.function(object, objectPropertyKeyList), listIndex)
    if (typeof objectMapResult === 'object' && (objectMapResult.objectPropertyKey || objectMapResult.objectPropertyValue)) {
      if (objectPropertyKeyList !== objectMapResult.propertyKey && objectMapResult.propertyKey !== '') {
        deleteObjectValue.function(object, objectPropertyKeyList)
      }
      objectPropertyKeyList = objectMapResult.objectPropertyKeyList || objectPropertyKeyList
      objectPropertyValue = objectMapResult.objectPropertyValue
    } else {
      objectPropertyValue = objectMapResult
    }
    return { objectPropertyKeyList, objectPropertyValue }
  })

  for (let objectMapResultListIndex = 0; objectMapResultListIndex < objectMapResultList.length; objectMapResultListIndex++) {
    updateObjectValue.function(object, objectMapResultList[objectMapResultListIndex].objectPropertyKeyList, objectMapResultList[objectMapResultListIndex].objectPropertyValue)
  }

  return object
}

// Constants

const updateObjectValueByMapFunction = {
  function: updateObjectValueByMapFunctionFunction
}

export {
  updateObjectValueByMapFunction
}
