
const webpackCommonConfig = require('./webpack.common.config')

module.exports = {
  ...webpackCommonConfig,
  target: 'node',
  mode: 'development',
  entry: {
    httpServer: './precompiled/http-server.ts'
  }
}

