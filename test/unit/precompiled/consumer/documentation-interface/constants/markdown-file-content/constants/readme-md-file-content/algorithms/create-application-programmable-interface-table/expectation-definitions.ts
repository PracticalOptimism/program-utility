

import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../../../../@expectation-definitions/algorithms/get-project-file-path'

import { createApplicationProgrammableInterfaceTable } from './index'

describe(getProjectFilePath.function(__filename), () => {
  it('should create a markdown dropdown list using <details> and <pre>', () => {
    const markdownText = createApplicationProgrammableInterfaceTable.function({
      documentationThing1: {
        documentationThing2: {
          documentationThing3: {
            documentationThingValue: true
          }
        }
      }
    })
    expect(markdownText).contains('<details>')
    expect(markdownText).contains('<pre>')
  })
})
