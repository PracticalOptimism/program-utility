
import { PROJECT_OPERATING_ENVIRONMENT_LIST } from '../../../../../../../../@project/constants/project'
import { createMarkdownTableFromJavascriptObject } from '../../../../../../../../usecase/documentation-utility/algorithms/markdown/create-markdown-table-from-javascript-object'

// Create Project Operating Environment Table
function createProjectOperatingEnvironmentTableFunction (): string {
  return PROJECT_OPERATING_ENVIRONMENT_LIST.map((projectOperatingEnvironmentObject) => {
    const tableTextName = `### ${projectOperatingEnvironmentObject.operatingEnvironmentTableName}`
    const tableTextDescription = createMarkdownTableFromJavascriptObject.function(projectOperatingEnvironmentObject.operatingEnvironmentTableObject)
    return `${tableTextName}\n\n${tableTextDescription}`
  }).join('\n\n')
}

const createProjectOperatingEnvironmentTable = {
  function: createProjectOperatingEnvironmentTableFunction
}

export {
  createProjectOperatingEnvironmentTable
}


