

import { getObjectPropertyKeyNameList, FunctionOption as GetObjectPropertyKeyNameListFunctionOption } from '../get-object-property-key-name-list'
import { getObjectValue } from '../get-object-value'

// Data Structures

class FunctionOption extends GetObjectPropertyKeyNameListFunctionOption {
  returnObjectPropertyKeyListPair?: boolean
  constructor (defaultOption?: FunctionOption) {
    super(defaultOption)
    Object.assign(this, defaultOption)
  }
}

declare type ObjectMap = { objectPropertyKeyList: (number | string)[], objectPropertyValue: any }

// Algorithms

function getObjectPropertyValueListFunction (object: any, option?: FunctionOption): (any | ObjectMap)[] {
  const propertyKeyNameList = getObjectPropertyKeyNameList.function(object, [], option).map((keyList: (number | string)[]) => {
    const objectPropertyValue = getObjectValue.function(object, keyList)
    if (option && option.returnObjectPropertyKeyListPair) {
      return { objectPropertyKeyList: keyList, objectPropertyValue }
    } else {
      return objectPropertyValue
    }
  })
  return propertyKeyNameList
}

// Constants

const getObjectPropertyValueList = {
  function: getObjectPropertyValueListFunction
}

export {
  getObjectPropertyValueList,
  FunctionOption,
  ObjectMap
}

