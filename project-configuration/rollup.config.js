
// rollup.config.js

// const typescript = require('@rollup/plugin-typescript')
// const resolve = require('@rollup/plugin-node-resolve')
// const path = require('path')

// import typescript from '@rollup/plugin-typescript'
// import resolve from '@rollup/plugin-node-resolve'

// const javascriptLibraryEntryFileLocation = `precompiled/consumer/javascript/algorithms/create-javascript-library/run-algorithm.ts`

// console.log('hello world 234')

// const rollupConfig = {
//   input: `precompiled/consumer/javascript/algorithms/create-javascript-library/run-algorithm.ts`,
//   output: [{
//     file: 'compiled/data-structures/javascript-library/index.js',
//     dir: 'compiled/data-structures/javascript-library',
//     format: 'umd'
//   }],
//   plugins: [
//     resolve({
//       extensions: ['.ts']
//     }),
//     typescript({
//       include: ["*.ts"],
//       tsconfig: 'tsconfig.json',
//       declaration: true
//     })
//   ]
// }

// module.exports = rollupConfig



import typescript from 'rollup-plugin-typescript2'
import commonjs from 'rollup-plugin-commonjs'
import resolve from '@rollup/plugin-node-resolve'
import json from '@rollup/plugin-json'
import { terser } from 'rollup-plugin-terser'
// import inject from '@rollup/plugin-inject'

export default {
  // input: `compiler/constants/rollup/example.ts`,
  input: `precompiled/javascript.ts`,
  output: [{
    name: 'programUtility',
    // file: 'compiled/yea/awesome.js',
    file: 'compiled/data-structures/javascript-library/javascript.js',
    format: 'esm'
  }],
  external: [
    'firebase',
    'localforage',
    'algoliasearch',
    'elliptic',
    // 'client-compress',
    'crypto',
    // 'jsdom',
    'window'
  ],
  // globals: {
  //   window: 'window'
  // },
  plugins: [
    // inject({
    //   // window: ['jsdom', 'JSDOM', 'new JSDOM()', 'window']
    // }),
    terser(),
    json(),
    resolve({
      mainFields: ['module', 'main'], // Default: ['module', 'main']
      module: true, // Default: true
      main: true,  // Default: true
      browser: true
    }),
    commonjs({
      include: [
        'node_modules/**'
        // 'node_modules/firebase/**',
        // 'node_modules/localforage/**',
        // 'node_modules/elliptic/**',
        // 'node_modules/client-compress/**',
        // 'node_modules/algoliasearch/**'
      ]
    }),
    typescript()
  ]
}

