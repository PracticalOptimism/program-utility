
import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../@expectation-definitions/algorithms/get-project-file-path'
import { updateObjectValueByMapFunction } from './index'

describe(getProjectFilePath.function(__filename), () => {
  it('should update an object by a map function', () => {
    const myObject = { englishName: 'Elizabeth', address: { city: 'Seoul, Korea' } }

    updateObjectValueByMapFunction.function(myObject, (objectPropertyKeyList: (number | string)[], objectPropertyValue: any) => {
      if (objectPropertyKeyList[0] === 'englishName') { return { objectPropertyKeyList: ['koreanName'], objectPropertyValue: 'Peiju' } }
      return objectPropertyValue
    })

    expect(myObject.address).to.deep.equal({ city: 'Seoul, Korea' })
    expect((myObject as any).koreanName).to.equal('Peiju')
    expect(myObject.englishName).to.equal(undefined)
  })
  it('should use property value of type array when that is the original value type', () => {
    const myObject = { englishName: 'Elizabeth', address: { city: 'Seoul, Korea' }, friendList: [{ name: 'Vladimir Putin' }, { name: 'Li Keqiang' }, { name: 'Jacque Fresco' }] }

    // Expect the original object property value to be of type 'Array'
    expect(Array.isArray(myObject.friendList)).to.equal(true)

    updateObjectValueByMapFunction.function(myObject, (objectPropertyKeyList: (number | string)[], objectPropertyValue: any) => {
      if (objectPropertyKeyList.join('.') !== 'friendList') {
        return objectPropertyValue
      }
      // Expect the updateObjectValueByMap callback parameter to be of type 'Array'
      expect(Array.isArray(objectPropertyValue)).to.equal(true)
    }, { objectPropertyTreeHeight: 1, shouldCreateDeepObjectClone: true })
  })
})

