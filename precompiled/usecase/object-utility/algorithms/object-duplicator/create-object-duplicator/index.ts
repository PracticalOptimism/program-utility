
import { ObjectDuplicator } from '../../../data-structures/object-duplicator'
import { createObjectDeepCopy } from '../../object/create-object-deep-copy'

// Algorithms

function createObjectDuplicatorFunction<ObjectType> (object: ObjectType): ObjectDuplicator<ObjectType> {
  return new ObjectDuplicator<ObjectType>(createObjectDeepCopy.function(object))
}

// Constants

const createObjectDuplicator = {
  function: createObjectDuplicatorFunction
}

export {
  createObjectDuplicator
}

