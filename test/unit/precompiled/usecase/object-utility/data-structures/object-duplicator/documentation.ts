

import * as indexFile from './index'

// Constants

const PROJECT_DOCUMENTATION_FILE_PATH_DIRECTORY_NAME = 'precompiled/usecase/object-utility/algorithms/object/create-object-deep-copy'

const PROJECT_ALGORITHM_NAME = 'getObjectValue'
const PROJECT_ALGORITHM_PARAMETER_LIST = [{
  parameterName: 'object',
  parameterType: 'any|Object',
  parameterExampleList: [`{ "name": "Patrick Morrison", "address": { "city": "Beijing" } }`]
}, {
  parameterName: 'propertyKeyNameList',
  parameterType: 'string[]',
  parameterExampleList: [`['address', 'city']`]
}]

const PROJECT_ALGORITHM_RETURN_PARAMETER_LIST = [{
  parameterName: 'objectPropertyValue',
  parameterType: 'any',
  parameterExampleList: [`"Beijing"`]
}]

const PROJECT_ALGORITHM_EXAMPLE_USECASE_LIST = [{
  projectExampleUsecaseTitle: `Getting Started: Function call example usecase`,
  projectExampleUsecaseSourceCodeTextDescription: `getObjectValue.function(object, propertyKeyNameList)`
}, {
  projectExampleUsecaseTitle: `Getting Started: Application context example usecase`,
  projectExampleTextDescription: `
  // Create a javascript library instance for your project
  const myProjectObjectUtility = objectUtility.consumer.javascript.algorithms.createJavascriptLibraryInstance.function('my-project-id')

  // Initialize a javascript object
  const myObject = { name: 'Claire', favoriteActivitiesTable: { dancing: { activityName: 'Dancing', placesVisitedToPerformActivity: ['New York', 'Los Angelos'], firstMemoryPerformingActivity: { placePerformedActivity: 'New York' } } }}

  // Retrieve the property value of myObject by using a property key name list
  const placePerformedActivityDuringFirstMemory = myProjectObjectUtility.usecase.objectUtility.algorithms.getObjectValue.function(myObject, ['favoriteActivitiesTable', 'dancing', 'firstMemoryPerformingActivity', 'placePerformedActivity'])

  // Print the place where the first memory of the dancing activity occurred as it is one of Claire's favorite activities
  console.log('The following should be "New York": ', placePerformedActivityDuringFirstMemory)`
}]

// Variables

let PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT = indexFile

// Documentation Topic

export {
  PROJECT_DOCUMENTATION_FILE_PATH_DIRECTORY_NAME,
  PROJECT_ALGORITHM_NAME,
  PROJECT_ALGORITHM_PARAMETER_LIST,
  PROJECT_ALGORITHM_RETURN_PARAMETER_LIST,
  PROJECT_ALGORITHM_EXAMPLE_USECASE_LIST,

  PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT
}
