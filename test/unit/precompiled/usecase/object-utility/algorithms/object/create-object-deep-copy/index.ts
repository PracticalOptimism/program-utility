import { getObjectValue } from '../get-object-value'
import { updateObjectValue } from '../update-object-value'


// Data Structures

class FunctionOption {
  deepObjectClonePropertyTreeHeight?: number
  constructor (defaultOption?: FunctionOption) {
    Object.assign(this, defaultOption)
  }
}

// Algorithms

function createObjectDeepCopyFunction (object: any, option?: FunctionOption): any {
  if (typeof object !== 'object') { return }

  if (option && option.deepObjectClonePropertyTreeHeight === 0) { return object }

  const newObject = {}

  let objectPropertyKeyNameList: Array<(number | string)[]> = []

  const originalObjectPropertyKeyNameList = Object.keys(object).map(objectPropertyKeyName => [objectPropertyKeyName])
  objectPropertyKeyNameList = objectPropertyKeyNameList.concat(originalObjectPropertyKeyNameList)

  for (let i = 0; i < objectPropertyKeyNameList.length; i++) {
    const propertyKeyNameList = objectPropertyKeyNameList[i]

    const objectPropertyValue = getObjectValue.function(object, propertyKeyNameList)

    // If the height of the tree is less than the height of the tree traversed so far or if the next item
    // in the tree hierarchy is an object and the height of the tree will be less than the height of those
    // object values, then add the objectPropertyValue from the original object to save object references
    if ((option && option.deepObjectClonePropertyTreeHeight !== undefined) &&
        ((propertyKeyNameList.length > option.deepObjectClonePropertyTreeHeight) ||
        ((typeof objectPropertyValue === 'object') && (propertyKeyNameList.length + 1 > option.deepObjectClonePropertyTreeHeight)))) {
      updateObjectValue.function(newObject, propertyKeyNameList, objectPropertyValue)
      continue
    }

    // Set value on new object only if a non-object value is reached, otherwise, add child keys to propertyKeyNameList
    if (typeof objectPropertyValue === 'object') {
      let childObjectPropertyKeyNameList
      if (Array.isArray(objectPropertyValue)) {
        childObjectPropertyKeyNameList = objectPropertyValue.map((_objectPropertyKeyValue, index) => [...propertyKeyNameList, index])
      } else {
        childObjectPropertyKeyNameList = Object.keys(objectPropertyValue).map(objectPropertyKeyName => [...propertyKeyNameList, objectPropertyKeyName])
      }
      objectPropertyKeyNameList = objectPropertyKeyNameList.concat(childObjectPropertyKeyNameList)
    } else {
      updateObjectValue.function(newObject, propertyKeyNameList, objectPropertyValue)
    }
  }

  return newObject
}

// Constants

const createObjectDeepCopy = {
  function: createObjectDeepCopyFunction
}

export {
  createObjectDeepCopy
}
