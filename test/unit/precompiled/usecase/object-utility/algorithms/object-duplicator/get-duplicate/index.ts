
import { ObjectDuplicator } from '../../../data-structures/object-duplicator'
import { createDuplicate } from '../create-duplicate'

class FunctionOption {
  booleanAddObjectIfUndefined?: boolean
  constructor (defaultOption?: FunctionOption) {
    Object.assign(this, defaultOption)
  }
}

// Algorithms

function getDuplicateFunction<ObjectType> (objectDuplicator: ObjectDuplicator<ObjectType>, duplicateId: string, option?: FunctionOption): ObjectType {

  let object = objectDuplicator.objectDuplicateTable[duplicateId]

  if (object === undefined && option && option.booleanAddObjectIfUndefined) {
    object = createDuplicate.function(objectDuplicator, duplicateId)
  }

  return object
}

// Constants

const getDuplicate = {
  function: getDuplicateFunction
}

export {
  getDuplicate
}

