
import { ObjectDuplicator } from '../../../data-structures/object-duplicator'
import { createObjectDeepCopy } from '../../object/create-object-deep-copy'

// Algorithms

function createDuplicateFunction<ObjectType> (objectDuplicator: ObjectDuplicator<ObjectType>, duplicateId: string): ObjectType {
  objectDuplicator.objectDuplicateTable[duplicateId] = createObjectDeepCopy.function(objectDuplicator.object)
  return objectDuplicator.objectDuplicateTable[duplicateId]
}

// Constants

const createDuplicate = {
  function: createDuplicateFunction
}

export {
  createDuplicate
}

