
import * as indexFile from './index'


// Constants
const PROJECT_DOCUMENTATION_TEXT_HEADER = 'updateObjectValue'

// Variables

let PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT = indexFile

// Documentation Topic

export {
  PROJECT_DOCUMENTATION_TEXT_HEADER,

  PROJECT_DOCUMENTATION_INDEX_FILE_CONTENT
}

