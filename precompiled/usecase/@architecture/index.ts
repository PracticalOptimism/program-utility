
import * as objectUtility from '../object-utility/@architecture'
import * as documentationUtility from '../documentation-utility/@architecture'

export {
  objectUtility,
  documentationUtility
}

